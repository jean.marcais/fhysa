import pandas as pd
from pandas.errors import EmptyDataError
#from matplotlib import pyplot as plt

def get_BankHydro(Input_folder,Station_Code,Area_BH):
    # instantiate Q_real_df at 0
    Q_real_df = pd.DataFrame()
    #Input_folder='/home/jean.marcais/Donnees/BanqueHydro/';
    #Area_BH=232
    f=open(Input_folder+Station_Code+'_qj_hydro2.txt', encoding='latin-1')
#     print(Input_folder+Station_Code+'_qj_hydro2.txt')
    # if watershed area is not provided (ie equal to -1), then the code reads it directly in the banque hydro file (hydrologic or topgraphic watershed)
    if(Area_BH==-1):
        Area_BH = get_area_BH_file(f)
        if(Area_BH==0):
            return Q_real_df
#         for j, line_j in enumerate(f):
#             if j == 2:
#                 Area_BH = line_j.split(";")[4]
#                 if(Area_BH):
#                     Area_BH = float(Area_BH)
#                 else:
#                     Area_BH = line_j.split(";")[5]
#                 if(Area_BH):
#                     Area_BH = float(Area_BH)
#                 if(not Area_BH or Area_BH=='0' or Area_BH==0):
#                     return Q_real_df

#     print(Area_BH)
    # read daily discharge in banque hydro file 
    # exclude other lines (monthly averaged discharge, informations, etc.)
    exclude = [i for i, line in enumerate(open(Input_folder+Station_Code+'_qj_hydro2.txt', encoding='latin-1')) if not line.startswith('QJO')]
    # reads file if no error
    try:
        Q_real_df=pd.read_csv(Input_folder+Station_Code+'_qj_hydro2.txt',skiprows=exclude,sep=";",header=None)
#     Q_real_df=pd.read_csv(Input_folder+Station_Code+'_qj_hydro2.txt',skiprows=[0,1,2,],sep=";",header=None, error_bad_lines=False)
#     Q_real_df=pd.read_csv(Input_folder+Station_Code+'_qj_hydro2.txt',skiprows=[i for i in range(0,118)],sep=";",header=None)
        Q_real_df=Q_real_df.rename(columns={2: "Datetime",3:"Q"})
        Q_real_df=Q_real_df[['Datetime','Q']]
        Q_real_df['Q']=Q_real_df['Q']*86400/(1e6*Area_BH)
        Q_real_df.drop(Q_real_df.tail(1).index,inplace=True)
        try:
            Q_real_df['Datetime'] =  pd.to_datetime(Q_real_df['Datetime'],format='%Y%m%d')
        except ValueError:
            Q_real_df = Q_real_df.drop(Q_real_df[Q_real_df['Datetime']==19000229].index)
            Q_real_df['Datetime'] =  pd.to_datetime(Q_real_df['Datetime'],format='%Y%m%d')
    # if errors return an empty dataframe
    except EmptyDataError:
        Q_real_df = pd.DataFrame()
    
#     if(Area_BH==0):
#         Q_real_df = pd.DataFrame()
    
    f.close()
    return Q_real_df
    #plt.plot(Q_real_df['Date'],Q_real_df['Q'])
    #plt.show()
    
def get_area_BH_file(f):
    if(type(f)==str):
        f=open(f, encoding='latin-1')
    
    Area_BH = 0
    for j, line_j in enumerate(f):
        if j == 2:
            Area_BH = line_j.split(";")[4]
            if(Area_BH and Area_BH!='0.000'):
                Area_BH = float(Area_BH)
            else:
                Area_BH = line_j.split(";")[5]
            if(Area_BH):
                Area_BH = float(Area_BH)
            if(not Area_BH):
                Area_BH = 0
    return Area_BH

def get_topo_and_BH_area(f):
    if(type(f)==str):
        f=open(f, encoding='latin-1')
    
    Area_BH = 0
    Area_Topo_BH = 0
    for j, line_j in enumerate(f):
        if j == 2:
            Area_BH = line_j.split(";")[4]
            if(Area_BH):
                Area_BH = float(Area_BH)
            elif(not Area_BH or Area_BH=='0'):
                Area_BH = 0
            else:
                Area_BH = 0
            Area_Topo_BH = line_j.split(";")[5]
            if(Area_Topo_BH and Area_Topo_BH!=' ' and not Area_Topo_BH.isspace()):
                Area_Topo_BH = float(Area_Topo_BH)
            elif(not Area_Topo_BH or Area_Topo_BH=='0'):
                Area_Topo_BH = 0
            else:
                Area_Topo_BH = 0
                    
                
    return Area_BH, Area_Topo_BH
            
def delete_HydroYear_withNan(df_obs, NaNvalues_number=20):
    print('There are ' + str(df_obs['Q'].isnull().sum()) + ' values missing in the observed discharge daily datasets. \n')
    print('We are going to delete Hydrological Years with more than ' + str(NaNvalues_number+1) + ' Nan Values and replace other by the average annual discharge. \n')
    # create a NaN column with 1 value for NaN values
    df_obs['NaN'] = df_obs['Q'].isnull().astype('int64')
    
    # create an Hydrological Date column with 1st of January corresponding to 1st of september -> does it mean "beginning of the hydrological year corresponding to 1st of september" ? 
    # df_obs['HydroDate'] = df_obs.index + pd.DateOffset(months=-8)
    df_obs['HydroDate'] = df_obs.Datetime + pd.DateOffset(months=-8)
    # delineate from that the Hydro Year
    df_obs['HydroYear'] = pd.DatetimeIndex(df_obs['HydroDate']).year
    # get yearly average with number of NaN values per year
    df_obs_year = df_obs.resample('AS', on='HydroDate').sum()
    
    full_discharge_timeseries = pd.DataFrame(df_obs)
    
    Hydro_Year_to_Delete = df_obs_year[df_obs_year.NaN > NaNvalues_number].index.year.values
    
    Hydro_Year_Information = df_obs_year['NaN']
    Hydro_Year_Information.index = Hydro_Year_Information.index.year
    
    if not Hydro_Year_to_Delete.any():
        print('Not too much Nan Values per year. So no Hydrological Year have been deleted from datasets \n')

    for Years in Hydro_Year_to_Delete:
        df_obs = df_obs.drop(df_obs[df_obs.HydroYear == Years].index)
        print('Hydrological Year '+str(Years)+' has been deleted. \n')

    Q_mean_annual = df_obs['Q'].mean()
    df_obs['Q'] = df_obs['Q'].fillna(Q_mean_annual)
    return df_obs, Hydro_Year_Information, Hydro_Year_to_Delete, full_discharge_timeseries