# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 14:38:24 2021

@author: laura.lindeperg
"""


import numpy as np
import pandas as pd
import rioxarray as rxr


class GeomorphologicProperties(object):
    def __init__(self, code=-1, ZHp_ratio=-1, intermittent_ratio=-1, drainage_density=-1, mean_elevation = -1, median_slope=-1, mean_DTB = -1):
        self.code = code
        self.ZHp_ratio = ZHp_ratio
        # self.order_1_ratio = order_1_ratio
        self.intermittent_ratio = intermittent_ratio
        self.drainage_density = drainage_density # [m-1]
        self.mean_elevation = mean_elevation # [m]
        self.median_slope = median_slope  # [%]
        self.mean_DTB = mean_DTB # [cm]
        


    def compute_ZHp_ratio(self, ZHp):
        # ZHp_test.values.flatten() -> fonction "unique" s'en occupe si ce n'est pas 1D
        classes, counts = np.unique(ZHp, return_counts=True)
        my_np_array = np.array([classes, counts]).transpose()
        ZHp_ClassAndCounts = pd.DataFrame(my_np_array, columns=['Classes', 'Counts'])
        ZHp_ClassAndCounts = ZHp_ClassAndCounts[ZHp_ClassAndCounts.loc[:, 'Classes'] != 65535] # remove pixels outside the studied area (class 65535)
        total_classes = ZHp_ClassAndCounts.loc[:,'Counts'].sum()
        ZHp_classes = ZHp_ClassAndCounts[ZHp_ClassAndCounts.loc[:, 'Classes'].isin([2,3,51])]
        ZHp_classes = ZHp_classes.loc[:,'Counts'].sum()
        ZHp_ratio = ZHp_classes/total_classes
        self.ZHp_ratio = ZHp_ratio

    def compute_order_1_ratio(self, hydro_sections):
        # Hydro sections - Total length
        total_length = hydro_sections.length.sum()
    
        # Keep order 1 only
        strahler_class_1 = hydro_sections[hydro_sections.loc[:,'GIGE_Sthr'] == 1]
        order_1_length = strahler_class_1.length.sum()
        
        order_1_ratio = order_1_length/total_length
        self.order_1_ratio = order_1_ratio
    
    def compute_intermittent_ratio(self, hydro_sections):
        # Hydro sections - Total length
        total_length = hydro_sections.length.sum()
        
        # Keep classes 1, 2 and 3 only (dry, ephemeral and intermittent)
        intermittent = hydro_sections[hydro_sections.loc[:,'Persistanc'].isin(['1','2','3'])]
        intermittent_length = intermittent.length.sum()
        
        intermittent_ratio = intermittent_length/total_length
        self.intermittent_ratio = intermittent_ratio
        
    def compute_persistence_nodata_ratio(hydro_sections):
        # Hydro sections - Total length
        total_length = hydro_sections.length.sum()
        
        # Compute ratio of NoData
        no_data = hydro_sections[hydro_sections.loc[:,'Persistanc'] == '0'] # 0 is the NoData class
        no_data_ratio = no_data.length.sum()/total_length
        return no_data_ratio
    
    def compute_drainage_density(self, contour, hydro_sections):
        # Hydro sections
        total_length = hydro_sections.length.sum()
        # Area
        surface_area = contour.geometry.area
        
        drainage_density = float(total_length/surface_area)
        self.drainage_density = drainage_density
    
    
    
    def extract_mean_value_from_raster(self, raster_path):
        raster = rxr.open_rasterio(raster_path, masked=True).squeeze()
        mean_value = np.nanmean(raster) # if masked = True when reading raster
        return mean_value
    
    def extract_median_value_from_raster(raster_path):
        raster = rxr.open_rasterio(raster_path, masked=True).squeeze()
        median_value = np.nanmedian(raster) # if masked = True when reading raster
        return median_value
    
    
    # def extract_mean_elevation(self, raster_path):
    #     self.mean_elevation = self.extract_mean_value_from_raster(raster_path)
        
    # def extract_median_slope(self, raster_path):
    #     self.median_slope = self.extract_median_value_from_raster(raster_path)
    
    # def extract_mean_DTB(self, raster_path):
    #     self.mean_DTB = self.extract_mean_value_from_raster(raster_path)
    
    def extract_mean_elevation(self, raster_path):
        raster = rxr.open_rasterio(raster_path, masked=True).squeeze()
        mean_value = np.nanmean(raster) # if masked = True when reading raster
        self.mean_elevation = mean_value
        
    def extract_median_slope(self, raster_path):
        raster = rxr.open_rasterio(raster_path, masked=True).squeeze()
        median_value = np.nanmedian(raster) # if masked = True when reading raster
        self.median_slope = median_value
    
    def extract_mean_DTB(self, raster_path):
        raster = rxr.open_rasterio(raster_path, masked=True).squeeze()
        mean_value = np.nanmean(raster) # if masked = True when reading raster
        self.mean_DTB = mean_value
    
    
    # def extract_mean_elevation(self, topographic_property_map):
    #     import numpy.ma as ma
    #     mean_elevation = np.mean(ma.masked_values(topographic_property_map, topographic_property_map._FillValue))
    #     self.mean_elevation = mean_elevation
        
    # def extract_median_slope(self, topographic_property_map):
    #     import numpy.ma as ma
    #     slope = ma.masked_values(topographic_property_map, topographic_property_map._FillValue)
    #     slope = slope.filled(np.nan)
    #     median_slope = np.nanmedian(slope)
    #     self.median_slope = median_slope
    
    # def extract_mean_DTB(self, DTB_map):
    #     import numpy.ma as ma
    #     mean_DTB = np.mean(ma.masked_values(DTB_map, DTB_map._FillValue)) # if masked = False when reading raster
    #     return mean_DTB

    # def extract_mean_DTB(self, DTB_map):
    #     mean_DTB = np.nanmean(DTB_map) # if masked = True when reading raster
    #     return mean_DTB
    
        
    def extract_geomorphologic_properties(self, contour, ZHp, hydro_sections_Topage, alti_path, slope_path, DTB_path):
        self.compute_ZHp_ratio(ZHp)
       # self.compute_order_1_ratio(hydro_sections_Carthage)
        self.compute_intermittent_ratio(hydro_sections_Topage)
        self.compute_drainage_density(contour, hydro_sections_Topage)
        self.extract_mean_elevation(alti_path)
        self.extract_median_slope(slope_path)
        self.extract_mean_DTB(DTB_path)
       
        
        
        
        
        
        
        
        
        
        
        