# FHySA

French Hydrological Signature Analysis. 
The project enables to retrieve different hydrological signatures and compare it with landscape attributes. Landscape attributes are retrieved from spatial analysis of different geo(morpho)logical controls.