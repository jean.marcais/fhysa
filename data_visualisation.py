# -*- coding: utf-8 -*-
"""
Created on Wed Apr  7 16:17:38 2021

@author: laura.lindeperg
"""



import geopandas as gpd
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pickle


# **************************** Data *****************************


# Load hydrological signatures df
df_hydro_sig_path = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/616_stations_hydrosig_df.csv'
# df_hydro_sig_path = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/616_stations_hydrosig_dim_df.csv'
df_hydro_sig = pd.read_csv(df_hydro_sig_path)

df_hydro_sig = df_hydro_sig.drop(columns = ['name'])



## Cross correlations - hydro signatures

df_hydro_sig_1 = df_hydro_sig.loc[:, ['code', 'aridity_ratio', 'q_mean', 'runoff_ratio']]
df_hydro_sig_2 = df_hydro_sig.loc[:, ['code', 'bfi_5', 'bfi_90', 'bf_magni']]
df_hydro_sig_3 = df_hydro_sig.loc[:, ['code', 'fdc_quantile10', 'fdc_quantile90', 'fdc_slope']]
df_hydro_sig_4 = df_hydro_sig.loc[:, ['code', 'tau_1', 'tau_2', 'tau_roques']]
df_hydro_sig_5 = df_hydro_sig.loc[:, ['code', 'a_q', 'b_q']]

df_hydro_sig_12 = pd.merge(df_hydro_sig_1, df_hydro_sig_2, on = 'code', how = 'outer')
df_hydro_sig_13 = pd.merge(df_hydro_sig_1, df_hydro_sig_3, on = 'code', how = 'outer')
df_hydro_sig_14 = pd.merge(df_hydro_sig_1, df_hydro_sig_4, on = 'code', how = 'outer')
df_hydro_sig_15 = pd.merge(df_hydro_sig_1, df_hydro_sig_5, on = 'code', how = 'outer')
df_hydro_sig_23 = pd.merge(df_hydro_sig_2, df_hydro_sig_3, on = 'code', how = 'outer')
df_hydro_sig_24 = pd.merge(df_hydro_sig_2, df_hydro_sig_4, on = 'code', how = 'outer')
df_hydro_sig_25 = pd.merge(df_hydro_sig_2, df_hydro_sig_5, on = 'code', how = 'outer')
df_hydro_sig_34 = pd.merge(df_hydro_sig_3, df_hydro_sig_4, on = 'code', how = 'outer')
df_hydro_sig_35 = pd.merge(df_hydro_sig_3, df_hydro_sig_5, on = 'code', how = 'outer')
df_hydro_sig_45 = pd.merge(df_hydro_sig_4, df_hydro_sig_5, on = 'code', how = 'outer')

df_hydro_sig_12_long = df_hydro_sig_12.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_13_long = df_hydro_sig_13.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_14_long = df_hydro_sig_14.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_15_long = df_hydro_sig_15.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_23_long = df_hydro_sig_23.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_24_long = df_hydro_sig_24.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_25_long = df_hydro_sig_25.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_34_long = df_hydro_sig_34.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_35_long = df_hydro_sig_35.melt(id_vars = ['code'], var_name = 'hydro_sig')
df_hydro_sig_45_long = df_hydro_sig_45.melt(id_vars = ['code'], var_name = 'hydro_sig')



# Watersheds
shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/616_Catchments.shp'
shp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'

shp_watersheds = gpd.read_file(shp_watersheds_path)

rrse_241_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/STATIONS/Stations241_RRSE.csv'
rrse_241 = pd.read_csv(rrse_241_path)
rrse_241 = rrse_241.rename(columns = {'levels.RRSE.':'Code'})
rrse_241 = rrse_241.loc[:, 'Code']


# # Other lists of stations
# r2se_236_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/STATIONS/R2SE_stations_et_critique_v01_IgnazioGiuntoli.xls'
# r2se_236 = pd.read_excel(r2se_236_path, sheet_name=0)
                         
# her_662_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/STATIONS/Stations662_HER.csv'
# her_662 = pd.read_csv(her_662_path)



# Geol
df_geol_path = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/616_stations_geol_df.csv'
df_geol = pd.read_csv(df_geol_path)

# Geomorpho
df_geomorpho_path = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/616_stations_geomorpho_df.csv'
df_geomorpho = pd.read_csv(df_geomorpho_path)





# Transform in long-form data structure
df_hydro_sig = df_hydro_sig.drop(columns = ['name'])
df_hydro_sig_long = df_hydro_sig.melt(id_vars = ['code'], var_name = 'hydro_sig')

my_df = pd.merge(df_hydro_sig_long, df_geol, on = 'code', how = 'outer')
# my_df = pd.merge(my_df, delta_v, on = 'code', how = 'outer')


# Merge hydrological signatures with geometry file
shp_watersheds = shp_watersheds.rename(columns = {'Code':'code'})
my_df = pd.merge(my_df, shp_watersheds, on = 'code', how = 'outer')




# Add maingeol_description from geol indicators in the df when using geomorpho indicators
geol = df_geol.loc[:, ['code', 'maingeol_description']]

df_hydro_sig_long = df_hydro_sig.melt(id_vars = ['code'], var_name = 'hydro_sig')

my_df = pd.merge(df_hydro_sig_long, df_geomorpho, on = 'code', how = 'outer')
my_df = pd.merge(my_df, geol, on='code')

# Pick catchments within the rrse list
my_df = my_df.loc[my_df.loc[:, 'code'].isin(rrse_241)==True]

# Filter border catchments
border_catchments = ['A3792010', 'A3902010', 'B4224310', 'B4601010', 'B4631010', 'D0156510', 'O1900010', 'O2620010', 'O6140010', 'U2512010', 'U2542010', 'U2722010','V1000010', 'V1020010']
my_df = my_df.loc[my_df.loc[:, 'code'].isin(border_catchments)==False]

# Filter catchments based on their size...
size = shp_watersheds.loc[shp_watersheds.loc[:, 'S_km2'] < 5000]
my_df = my_df.loc[my_df.loc[:, 'code'].isin(size.loc[:,'Code'])==True]
# ...and their main geol proportion
proportion_geol = df_geol.loc[df_geol.loc[:, 'maingeol_proportion'] > 0.70]
my_df = my_df.loc[my_df.loc[:, 'code'].isin(proportion_geol.loc[:,'code'])==True]

# Exculde catchments with weird hydrographs
weird_catchments = pd.DataFrame(np.array(['H6201010', 'L4010710', 'Y1232010']), columns=['Code'])
weird_catchments=weird_catchments.loc[:,'Code']
my_df = my_df.loc[my_df.loc[:, 'code'].isin(weird_catchments)==False]

# # Exclude catchments which disturb scales
outlier_catchments = ['P5404010'] # outlier (value of a_l) in dimensioned version
my_df = my_df.loc[my_df.loc[:,'code'].isin(outlier_catchments) == False]
 
# outlier_catchments = ['V2814020']
# outlier_catchments = ['V2814020', 'V5045020', 'H4033010', 'S4214010']
# my_df = my_df.loc[my_df.loc[:,'code'].isin(['K6492510', 'V2814020', 'P7041510', 'A9001050', 'H8043310', 'H4033010']) == False]


i = outlier_catchments[3]
filename_i = 'watershed_'+i
infile_i = open(filename_i, 'rb')
my_watershed = pickle.load(infile_i)
infile_i.close()

timeseries_q_V2814020 = my_watershed.hydro_climatic_fluxes.discharge_timeseries
timeseries_q_V5045020 = my_watershed.hydro_climatic_fluxes.discharge_timeseries
timeseries_q_H4033010 = my_watershed.hydro_climatic_fluxes.discharge_timeseries
timeseries_q_S4214010 = my_watershed.hydro_climatic_fluxes.discharge_timeseries

g = sns.relplot(x="Datetime", y="Q", kind="line", data=timeseries_q_S4214010)
g.fig.autofmt_xdate()

# **************************** Plots *****************************

## Discharge timeseries 

# List of the stations'codes
watershed_code = shp_watersheds.loc[:,'Code']

code_from_rrse = rrse_241.loc[rrse_241.isin(watershed_code) == True]

# code_from_her = watershed_code.loc[watershed_code.isin(her_662.loc[:, 'Num']) == True]

# r2se_codes = r2se_236.loc[1:236, 'Code']
# code_from_r2se = watershed_code.loc[watershed_code.isin(r2se_codes) == True]

# rrse_her = rrse_241.loc[rrse_241.isin(her_662.loc[:, 'Num']) == True]
# rrse_r2se = r2se_codes.loc[r2se_codes.isin(rrse_241) == True]
# her_r2se = r2se_codes.loc[r2se_codes.isin(her_662.loc[:, 'Num']) == True]


# fromRRSE_fromR2SE = code_from_rrse.loc[code_from_rrse.isin(code_from_r2se) == True]
# fromRRSE_fromHER = code_from_rrse.loc[code_from_rrse.isin(code_from_her) == True]
# from_RRSE_R2SE = rrse_r2se.loc[rrse_r2se.isin(watershed_code) == True]
# from_RRSE_HER = rrse_her.loc[rrse_her.isin(watershed_code) == True]
# from_HER_R2SE = her_r2se.loc[her_r2se.isin(watershed_code) == True]

# final_check = from_HER_R2SE.loc[from_HER_R2SE.isin(code_from_rrse) == True]


weird_catchments = pd.DataFrame(np.array(['H6201010', 'L4010710', 'Y1232010']), columns=['Code'])
weird_catchments=weird_catchments.loc[:,'Code']
# weird_catchments.isin(final_check) # -> all True

# duplicated_stations = code_from_r2se.duplicated() # -> True for the second occurrence in Code column
# index_duplicated = duplicated_stations[duplicated_stations[:,] == True]
# stations_duplicated = my_df_stations.iloc[index_duplicated.index[0]]

# # And remove them (the second occurrence is removed)
# no_duplicated = my_df_stations.drop(index_duplicated.index[0])

# Get a sample of them for test
code_for_test = watershed_code.loc[0:2]

# Open stored watershed objects
# for i in watershed_code:
# for i in odd_catchments:
for i in code_for_test:
# for i in code_from_rrse:
    # filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/watershed_'+i
    # filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/watershed_allYears_'+i
    # filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/Adim_new/watershed_adim_'+i
    # filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/Dim/watershed_dim_J4742020'
    filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/Dim/watershed_dim_'+i
    infile_i = open(filename_i, 'rb')
    watershed_i = pickle.load(infile_i)
    infile_i.close()
    
    # timeseries_q_i = watershed_i.hydro_climatic_fluxes.discharge_timeseries
    area = str(watershed_i.contour['S_km2'][0])
    
    deleted_years = watershed_i.hydro_climatic_fluxes.deleted_Hydro_Year
    entire_timeseries = watershed_i.hydro_climatic_fluxes.entire_q_timeseries
    where_removed_year = entire_timeseries['HydroDate'].dt.year.isin(deleted_years)
    timeseries_q_i = pd.DataFrame(entire_timeseries)
    # timeseries_q_i['Q'] = timeseries_q_i['Q'].mask(where_removed_year)
    timeseries_q_i['Q'] = timeseries_q_i['Q'].where(~where_removed_year, np.nan)
    removed_years = pd.DataFrame(entire_timeseries['Datetime'].where(where_removed_year)).dropna()
    # removed_years = pd.to_datetime(removed_years['Datetime'])

    # g = sns.relplot(x="Datetime", y="Q", kind = 'line', data= timeseries_q_i).set(title='watershed '+i+'  Area = '+area+ ' km²', ylabel= 'Q [mm/j]')
    # # g = sns.relplot(x="Datetime", y="Q", kind = 'line', data= timeseries_q_i).set(title='watershed '+i+'  Area = '+area+ ' km²', ylabel= 'Q/Qmedian')
    # plt.vlines(x = removed_years.Datetime, ymin = 0, ymax =timeseries_q_i.Q.max(), color = 'mistyrose')
    # g.fig.autofmt_xdate()
   
    plt.close()
    plt.plot(np.array(timeseries_q_i.Datetime), np.array(timeseries_q_i.Q))
    # plt.yscale('log')
    plt.vlines(x = removed_years.Datetime, ymin = 0, ymax =timeseries_q_i.Q.max(), color = 'mistyrose')
    plt.xlabel('Datetime')
    plt.ylabel('Q [mm/j]')
    # plt.ylabel('Q/Qmedian')
    plt.title(label='watershed '+i+'  Area = '+area+ ' km²')
    plt.show()
    
    plt.savefig('C:/Users/laura.lindeperg/Documents/INRAE_2021/FIGURES/Discharge_timeseries/Dim/616_Stations/discharge_timeseries_dim_'+i+'.png', dpi=400, facecolor='w',edgecolor='w', format='png', pad_inches=0.1)



## Hydro sig correlations

geol = df_geol.loc[:, ['code', 'maingeol_description']]
my_data = pd.merge(df_hydro_sig_45, geol)

# Filtering workflow
my_data = my_data.loc[my_data.loc[:, 'code'].isin(rrse_241)==True]
my_data = my_data.loc[my_data.loc[:, 'code'].isin(border_catchments)==False]
my_data = my_data.loc[my_data.loc[:, 'code'].isin(size.loc[:,'Code'])==True]
my_data = my_data.loc[my_data.loc[:, 'code'].isin(proportion_geol.loc[:,'code'])==True]
# my_data = my_data.loc[my_data.loc[:, 'code'].isin(outlier_catchments)==False]
my_data = my_data.loc[my_data.loc[:, 'code'].isin(weird_catchments)==False]
my_data=my_data.sort_values(by=['maingeol_description'])

viz = sns.PairGrid(data=my_data, hue='maingeol_description')
viz.map(sns.scatterplot)
viz.add_legend()

viz.savefig('C:/Users/laura.lindeperg/Documents/INRAE_2021/FIGURES/Hydrological_signatures/Hydro_sig_correlation_RRSE_BordersSizeGeolWeird_45.png', dpi=400, facecolor='w',edgecolor='w', format='png', pad_inches=0.1)

# viz.map_diag(sns.histplot)
# viz.map_offdiag(sns.scatterplot)
# viz.add_legend()

my_df = my_df.loc[my_df.loc[:, 'code'].isin(outlier_catchments)==False]
my_df = my_df.loc[my_df.loc[:, 'code'].isin(weird_catchments)==False]
my_df = my_df.loc[my_df.loc[:, 'code'].isin(code_from_rrse)==True]
# medians = my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean'].groupby(['maingeol_description'])['value'].median().values
nb_bv_geol = my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']['maingeol_description'].value_counts().sort_index()
nobs = nb_bv_geol.values
nobs = [str(x) for x in nobs.tolist()]
nobs = [i for i in nobs]
# nobs = ["n: " + i for i in nobs]






my_df=my_df.sort_values(by=['maingeol_description'])


## Scatterplots of the hydrological signatures with extra dimension -> geol or geomorpho indicator parameter

x_column = 'median_slope'
y_column = 'value'
define_hue = 'maingeol_description'
hydrosig_list = ['bfi_5', 'bfi_90', 'b_q', 'tau_roques', 'tau_2', 'fdc_slope']
xtitle = 'median slope [%]'

from scipy.stats import spearmanr

#calculate Spearman Rank correlation and corresponding p-value
my_data = pd.merge(df_hydro_sig, df_geomorpho, on = 'code', how = 'outer')

spearman_coeff = pd.DataFrame(columns=['hydro_sig', 'rho', 'p_value'])
for i in hydrosig_list:
    rho, p = spearmanr(my_data[x_column], my_data[i], nan_policy='omit')
    spearman_coeff = spearman_coeff.append({'hydro_sig':i, 'rho': rho, 'p_value': p}, ignore_index = True)


figure, axes = plt.subplots(2, 3, figsize = (17, 17), sharex = True)
# figure.suptitle('Hydrological signatures')
axes[0, 0].set_title('BFI 5 \nrho: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'bfi_5'].rho.values[0]) + '   p: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'bfi_5'].p_value.values[0]))
axes[0, 1].set_title('BFI 90 \nrho: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'bfi_90'].rho.values[0]) + '   p: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'bfi_90'].p_value.values[0]))
axes[0, 2].set_title('b_l Roques \nrho: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'b_q'].rho.values[0]) + '   p: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'b_q'].p_value.values[0]))
axes[1, 0].set_title('tau_l Roques \nrho: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'tau_roques'].rho.values[0]) + '   p: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'tau_roques'].p_value.values[0]))
axes[1, 1].set_title('tau_l Horner \nrho: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'tau_2'].rho.values[0]) + '   p: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'tau_2'].p_value.values[0]))
axes[1, 2].set_title('FDC slope \nrho: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'fdc_slope'].rho.values[0]) + '   p: ' + str("%.4f"%spearman_coeff.loc[spearman_coeff.hydro_sig == 'fdc_slope'].p_value.values[0]))
# axes[1, 2].set_title('FDC q90')


sns.scatterplot(ax=axes[0, 0], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_5']).set(xlabel = None, ylabel = None)
sns.scatterplot(ax=axes[0, 1], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_90']).set(xlabel = None, ylabel = None)
sns.scatterplot(ax=axes[0, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'b_q']).set(xlabel = None, ylabel = None)
sns.scatterplot(ax=axes[1, 0], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_roques']).set(ylabel = None, xlabel = xtitle)
sns.scatterplot(ax=axes[1, 1], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_2']).set(ylabel = None, xlabel = xtitle)
sns.scatterplot(ax=axes[1, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_slope']).set(ylabel = None, xlabel = xtitle)
# sns.scatterplot(ax=axes[1, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile90']).set(xlabel = None, ylabel = None, xscale = 'log')
# ax31.set(xlabel = None, ylabel = None)
axes[0,0].legend().set_visible(False)
axes[0,1].legend().set_visible(False)
axes[0,2].legend().set_visible(False)
axes[1,0].legend().set_visible(False)
axes[1,1].legend().set_visible(False)
axes[1,2].legend().set_visible(False)
# axes[1,2].legend().set_visible(False)
# ax31.set_xticklabels('')
# axes[2, 0].axis("off")
# axes[2, 1].axis("off")
# axes[2, 2].axis("off")

import matplotlib.patches as mpatches

mont = mpatches.Patch(color=sns.color_palette("deep")[0], label = nb_bv_geol.index[0] + ' - ' + nobs[0])
socle_aquif = mpatches.Patch(color= sns.color_palette("deep")[1], label = nb_bv_geol.index[1] + ' - ' + nobs[1])
socle_imper = mpatches.Patch(color= sns.color_palette("deep")[2], label = nb_bv_geol.index[2] + ' - ' + nobs[2])
socle_semiper = mpatches.Patch(color= sns.color_palette("deep")[3], label = nb_bv_geol.index[3] + ' - ' + nobs[3])
sedim_aqu_karst = mpatches.Patch(color= sns.color_palette("deep")[4], label = nb_bv_geol.index[4] + ' - ' + nobs[4])
sedim_aqu_non_karst = mpatches.Patch(color= sns.color_palette("deep")[5], label = nb_bv_geol.index[5] + ' - ' + nobs[5])
sedim_imper = mpatches.Patch(color= sns.color_palette("deep")[6], label = nb_bv_geol.index[6] + ' - ' + nobs[6])
sedim_semiper_karst = mpatches.Patch(color= sns.color_palette("deep")[7], label = nb_bv_geol.index[7] + ' - ' + nobs[7])
sedim_semiper_non_karst = mpatches.Patch(color= sns.color_palette("deep")[8], label = nb_bv_geol.index[8] + ' - ' + nobs[8])
volc = mpatches.Patch(color= sns.color_palette("deep")[9], label = nb_bv_geol.index[9] + ' - ' + nobs[9])

plt.legend(handles=[mont, socle_aquif, socle_imper, socle_semiper, sedim_aqu_karst, sedim_aqu_non_karst, sedim_imper, sedim_semiper_karst, sedim_semiper_non_karst, volc], loc='upper center', bbox_to_anchor=(0.5, -0.05))








## Boxplots of the hydrological signatures with extra dimension -> geol or geomorpho indicator parameter

x_column = 'maingeol_description'
y_column = 'value'
define_hue = 'maingeol_description'


figure, axes = plt.subplots(2, 3, figsize = (17, 17), sharex = True)
# figure.suptitle('Hydrological signatures')
axes[0, 0].set_title('BFI 5')
axes[0, 1].set_title('BFI 90')
axes[0, 2].set_title('b_l Roques')
axes[1, 0].set_title('tau_l Roques')
axes[1, 1].set_title('tau_l Horner')
axes[1, 2].set_title('FDC slope')
# axes[1, 2].set_title('FDC q90')


sns.boxplot(ax=axes[0, 0], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_5']).set(xlabel = None, ylabel = None)
sns.boxplot(ax=axes[0, 1], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_90']).set(xlabel = None, ylabel = None)
sns.boxplot(ax=axes[0, 2], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'b_q']).set(xlabel = None, ylabel = None)
ax10 = sns.boxplot(ax=axes[1, 0], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_roques']).set(xlabel = None, ylabel = None)
sns.boxplot(ax=axes[1, 1], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_2']).set(xlabel = None, ylabel = None)
sns.boxplot(ax=axes[1, 2], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_slope']).set(xlabel = None, ylabel = None)
# sns.scatterplot(ax=axes[1, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile90']).set(xlabel = None, ylabel = None)
# ax31.set(xlabel = None, ylabel = None)
# axes[0,0].legend().set_visible(False)
# axes[0,1].legend().set_visible(False)
# axes[0,2].legend().set_visible(False)
# axes[1,0].legend().set_visible(False)
# axes[1,1].legend().set_visible(False)
# axes[1,2].legend().set_visible(False)
# axes[1,2].legend().set_visible(False)

# axes[1,2].set(xlabel = None, ylabel = None)
# axes[1,0].set_xticklabels('')
# axes[1,1].set_xticklabels('')
axes[1,2].set_xticklabels('')
# axes[2, 0].axis("off")
# axes[2, 1].axis("off")
# axes[2, 2].axis("off")

import matplotlib.patches as mpatches

mont = mpatches.Patch(color=sns.color_palette("deep")[0], label = nb_bv_geol.index[0] + ' - ' + nobs[0])
socle_aquif = mpatches.Patch(color= sns.color_palette("deep")[1], label = nb_bv_geol.index[1] + ' - ' + nobs[1])
socle_imper = mpatches.Patch(color= sns.color_palette("deep")[2], label = nb_bv_geol.index[2] + ' - ' + nobs[2])
socle_semiper = mpatches.Patch(color= sns.color_palette("deep")[3], label = nb_bv_geol.index[3] + ' - ' + nobs[3])
sedim_aqu_karst = mpatches.Patch(color= sns.color_palette("deep")[4], label = nb_bv_geol.index[4] + ' - ' + nobs[4])
sedim_aqu_non_karst = mpatches.Patch(color= sns.color_palette("deep")[5], label = nb_bv_geol.index[5] + ' - ' + nobs[5])
sedim_imper = mpatches.Patch(color= sns.color_palette("deep")[6], label = nb_bv_geol.index[6] + ' - ' + nobs[6])
sedim_semiper_karst = mpatches.Patch(color= sns.color_palette("deep")[7], label = nb_bv_geol.index[7] + ' - ' + nobs[7])
sedim_semiper_non_karst = mpatches.Patch(color= sns.color_palette("deep")[8], label = nb_bv_geol.index[8] + ' - ' + nobs[8])
volc = mpatches.Patch(color= sns.color_palette("deep")[9], label = nb_bv_geol.index[9] + ' - ' + nobs[9])

plt.legend(handles=[mont, socle_aquif, socle_imper, socle_semiper, sedim_aqu_karst, sedim_aqu_non_karst, sedim_imper, sedim_semiper_karst, sedim_semiper_non_karst, volc], loc='upper center', bbox_to_anchor=(0.5, -0.05))






























## Scatterplots of the hydrological signatures with extra dimension -> geol or geomorpho indicator parameter

x_column = 'ks'
y_column = 'value'
define_hue = 'maingeol_description'


figure, axes = plt.subplots(4, 4, figsize = (17, 17), sharex = True)
# figure.suptitle('Hydrological signatures')
axes[0, 0].set_title('Qmean')
axes[0, 1].set_title('Aridity ratio')
axes[0, 2].set_title('Runoff ratio')
axes[0, 3].set_title('BFI 5')
axes[1, 0].set_title('BFI 90')
axes[1, 1].set_title('BF magni')
axes[1, 2].set_title('a_l Roques')
axes[1, 3].set_title('b_l Roques')
axes[2, 0].set_title('FDC q10')
axes[2, 1].set_title('FDC q90')
axes[2, 2].set_title('FDC slope')
axes[2, 3].set_title('tau_e Horner')
axes[3, 0].set_title('tau_l Horner ')
axes[3, 1].set_title('tau_l Roques')

ax00 = sns.scatterplot(ax=axes[0, 0], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']).set(xlabel = None, ylabel = None)
ax01 = sns.scatterplot(ax=axes[0, 1], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'aridity_ratio']).set(xlabel = None, ylabel = None)
ax02 = sns.scatterplot(ax=axes[0, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'runoff_ratio']).set(xlabel = None, ylabel = None)
ax03 = sns.scatterplot(ax=axes[0, 3], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_5']).set(xlabel = None, ylabel = None)
ax10 = sns.scatterplot(ax=axes[1, 0], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_90']).set(xlabel = None, ylabel = None)
ax11 = sns.scatterplot(ax=axes[1, 1], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bf_magni']).set(xlabel = None, ylabel = None)
ax12 = sns.scatterplot(ax=axes[1, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'a_q']).set(xlabel = None, ylabel = None)
ax13 = sns.scatterplot(ax=axes[1, 3], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'b_q']).set(xlabel = None, ylabel = None)
ax20 = sns.scatterplot(ax=axes[2, 0], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile10']).set(xlabel = None, ylabel = None)
ax21 = sns.scatterplot(ax=axes[2, 1], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile90']).set(xlabel = None, ylabel = None)
ax22 = sns.scatterplot(ax=axes[2, 2], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_slope']).set(xlabel = None, ylabel = None)
ax23 = sns.scatterplot(ax=axes[2, 3], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_1']).set(xlabel = None, ylabel = None)
ax30 = sns.scatterplot(ax=axes[3, 0], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_2']).set(xlabel = None, ylabel = None)
ax31 = sns.scatterplot(ax=axes[3, 1], x=x_column, y=y_column, hue = define_hue, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_roques'])
ax31.set(xlabel = None, ylabel = None)
axes[0,0].legend().set_visible(False)
axes[0,1].legend().set_visible(False)
axes[0,2].legend().set_visible(False)
axes[0,3].legend().set_visible(False)
axes[1,0].legend().set_visible(False)
axes[1,1].legend().set_visible(False)
axes[1,2].legend().set_visible(False)
axes[1,3].legend().set_visible(False)
axes[2,0].legend().set_visible(False)
axes[2,1].legend().set_visible(False)
axes[2,2].legend().set_visible(False)
axes[2,3].legend().set_visible(False)
axes[3,0].legend().set_visible(False)
axes[3,1].legend().set_visible(False)
# ax31.set_xticklabels('')
axes[3, 2].axis("off")
axes[3, 3].axis("off")


import matplotlib.patches as mpatches

mont = mpatches.Patch(color=sns.color_palette("deep")[0], label = nb_bv_geol.index[0] + ' - ' + nobs[0])
socle_aquif = mpatches.Patch(color= sns.color_palette("deep")[1], label = nb_bv_geol.index[1] + ' - ' + nobs[1])
socle_imper = mpatches.Patch(color= sns.color_palette("deep")[2], label = nb_bv_geol.index[2] + ' - ' + nobs[2])
socle_semiper = mpatches.Patch(color= sns.color_palette("deep")[3], label = nb_bv_geol.index[3] + ' - ' + nobs[3])
sedim_aqu_karst = mpatches.Patch(color= sns.color_palette("deep")[4], label = nb_bv_geol.index[4] + ' - ' + nobs[4])
sedim_aqu_non_karst = mpatches.Patch(color= sns.color_palette("deep")[5], label = nb_bv_geol.index[5] + ' - ' + nobs[5])
sedim_imper = mpatches.Patch(color= sns.color_palette("deep")[6], label = nb_bv_geol.index[6] + ' - ' + nobs[6])
sedim_semiper_karst = mpatches.Patch(color= sns.color_palette("deep")[7], label = nb_bv_geol.index[7] + ' - ' + nobs[7])
sedim_semiper_non_karst = mpatches.Patch(color= sns.color_palette("deep")[8], label = nb_bv_geol.index[8] + ' - ' + nobs[8])
volc = mpatches.Patch(color= sns.color_palette("deep")[9], label = nb_bv_geol.index[9] + ' - ' + nobs[9])

plt.legend(handles=[mont, socle_aquif, socle_imper, socle_semiper, sedim_aqu_karst, sedim_aqu_non_karst, sedim_imper, sedim_semiper_karst, sedim_semiper_non_karst, volc])








## Boxplots of the hydrological signatures with extra dimension -> maingeol_description parameter


x_column = 'maingeol_description'
y_column = 'value'
define_hue = 'maingeol-description'


figure, axes = plt.subplots(4, 4, figsize = (17, 17), sharex = True)
# figure.suptitle('Hydrological signatures')
axes[0, 0].set_title('Qmean')
axes[0, 1].set_title('Aridity ratio')
axes[0, 2].set_title('Runoff ratio')
axes[0, 3].set_title('BFI 5')
axes[1, 0].set_title('BFI 90')
axes[1, 1].set_title('BF magni')
axes[1, 2].set_title('a_l Roques')
axes[1, 3].set_title('b_l Roques')
axes[2, 0].set_title('FDC q10')
axes[2, 1].set_title('FDC q90')
axes[2, 2].set_title('FDC slope')
axes[2, 3].set_title('tau_e Horner')
axes[3, 0].set_title('tau_l Horner ')
axes[3, 1].set_title('tau_l Roques')

ax00 = sns.boxplot(ax=axes[0, 0], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']).set(xlabel = None, ylabel = None)
ax01 = sns.boxplot(ax=axes[0, 1], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'aridity_ratio']).set(xlabel = None, ylabel = None)
ax02 = sns.boxplot(ax=axes[0, 2], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'runoff_ratio']).set(xlabel = None, ylabel = None)
ax03 = sns.boxplot(ax=axes[0, 3], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_5']).set(xlabel = None, ylabel = None)
ax10 = sns.boxplot(ax=axes[1, 0], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_90']).set(xlabel = None, ylabel = None)
ax11 = sns.boxplot(ax=axes[1, 1], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bf_magni']).set(xlabel = None, ylabel = None)
ax12 = sns.boxplot(ax=axes[1, 2], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'a_q']).set(xlabel = None, ylabel = None)
ax13 = sns.boxplot(ax=axes[1, 3], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'b_q']).set(xlabel = None, ylabel = None)
ax20 = sns.boxplot(ax=axes[2, 0], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile10']).set(xlabel = None, ylabel = None)
ax21 = sns.boxplot(ax=axes[2, 1], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile90']).set(xlabel = None, ylabel = None)
ax22 = sns.boxplot(ax=axes[2, 2], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_slope']).set(xlabel = None, ylabel = None)
ax23 = sns.boxplot(ax=axes[2, 3], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_1']).set(xlabel = None, ylabel = None)
ax30 = sns.boxplot(ax=axes[3, 0], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_2']).set(xlabel = None, ylabel = None)
ax31 = sns.boxplot(ax=axes[3, 1], x=x_column, y=y_column, data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_roques'])
ax31.set(xlabel = None, ylabel = None)
ax31.set_xticklabels('')
axes[3, 2].axis("off")
axes[3, 3].axis("off")

# # Add it to the plot
# pos = range(len(nobs))
# # for tick,label in zip(pos,ax31.get_xticklabels()):
# for tick,label in zip(pos,nb_bv_geol.index):
#     ax00.text(pos[tick],
#             6,
#             nobs[tick],
#             horizontalalignment='center',
#             fontsize='x-small',
#             color='black',
#             fontweight='semibold')
    
# legend

import matplotlib.patches as mpatches

mont = mpatches.Patch(color=sns.color_palette("deep")[0], label = nb_bv_geol.index[0] + ' - ' + nobs[0])
socle_aquif = mpatches.Patch(color= sns.color_palette("deep")[1], label = nb_bv_geol.index[1] + ' - ' + nobs[1])
socle_imper = mpatches.Patch(color= sns.color_palette("deep")[2], label = nb_bv_geol.index[2] + ' - ' + nobs[2])
socle_semiper = mpatches.Patch(color= sns.color_palette("deep")[3], label = nb_bv_geol.index[3] + ' - ' + nobs[3])
sedim_aqu_karst = mpatches.Patch(color= sns.color_palette("deep")[4], label = nb_bv_geol.index[4] + ' - ' + nobs[4])
sedim_aqu_non_karst = mpatches.Patch(color= sns.color_palette("deep")[5], label = nb_bv_geol.index[5] + ' - ' + nobs[5])
sedim_imper = mpatches.Patch(color= sns.color_palette("deep")[6], label = nb_bv_geol.index[6] + ' - ' + nobs[6])
sedim_semiper_karst = mpatches.Patch(color= sns.color_palette("deep")[7], label = nb_bv_geol.index[7] + ' - ' + nobs[7])
sedim_semiper_non_karst = mpatches.Patch(color= sns.color_palette("deep")[8], label = nb_bv_geol.index[8] + ' - ' + nobs[8])
volc = mpatches.Patch(color= sns.color_palette("deep")[9], label = nb_bv_geol.index[9] + ' - ' + nobs[9])

plt.legend(handles=[mont, socle_aquif, socle_imper, socle_semiper, sedim_aqu_karst, sedim_aqu_non_karst, sedim_imper, sedim_semiper_karst, sedim_semiper_non_karst, volc])











figure.legend([ax01.legend],             # List of the line objects
           labels= nb_bv_geol.index,       # The labels for each line
           #loc='best',        # Position of the legend
           borderaxespad=0.1,         # Add little spacing around the legend box
           title="Legend Title")      # Title for the legend


figure.tight_layout()

sns.boxplot(x='maingeol_description', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']).tick_params(axis='x', labelrotation=45)










## Scatterplot - geographic repartition
figure, axes = plt.subplots(4, 4, figsize = (17, 17))
figure.suptitle('Hydrological signatures')
axes[0, 0].set_title('Qmean')
axes[0, 1].set_title('Aridity ratio')
axes[0, 2].set_title('Runoff ratio')
axes[0, 3].set_title('BFI 5')
axes[1, 0].set_title('BF magni')
axes[1, 1].set_title('a_q')
axes[1, 2].set_title('b_q')
axes[1, 3].set_title('FDC q10')
axes[2, 0].set_title('FDC q90')
axes[2, 1].set_title('FDC slope')
axes[2, 2].set_title('tau 1')
axes[2, 3].set_title('tau 2')
axes[3, 0].set_title('tau Roques')
axes[3, 1].set_title('BFI 90')

sns.scatterplot(ax=axes[0, 0], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[0, 1], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'aridity_ratio']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[0, 2], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'runoff_ratio']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[0, 3], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_5']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[1, 0], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bf_magni']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[1, 1], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'a_q']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[1, 2], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'b_q']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[1, 3], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile10']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[2, 0], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile90']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[2, 1], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_slope']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[2, 2], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_1']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[2, 3], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_2']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[3, 0], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_roques']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])
sns.scatterplot(ax=axes[3, 1], x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_90']).set(xlabel = None, ylabel = None, xticks=[], yticks = [])

axes[3, 2].axis("off")
axes[3, 3].axis("off")




# my_df.value=np.log(my_df.value)
sns.relplot(x='DeltaV', y='DeltaV', hue = 'maingeol_description', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean'])
sns.boxplot(x='maingeol_description', y='DeltaV', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']).tick_params(axis='x', labelrotation=45)




## Boxplots of the hydrological signatures

figure, axes = plt.subplots(4, 4, figsize = (17, 17))
# figure.suptitle('Hydrological signatures')
axes[0, 0].set_title('Qmean')
axes[0, 1].set_title('Aridity ratio')
axes[0, 2].set_title('Runoff ratio')
axes[0, 3].set_title('BFI 5')
axes[1, 0].set_title('BF magni')
axes[1, 1].set_title('a_q')
axes[1, 2].set_title('b_q')
axes[1, 3].set_title('FDC q10')
axes[2, 0].set_title('FDC q90')
axes[2, 1].set_title('FDC slope')
axes[2, 2].set_title('tau 1')
axes[2, 3].set_title('tau 2')
axes[3, 0].set_title('tau Roques')
axes[3, 1].set_title('BFI 90')

sns.boxplot(ax=axes[0, 0], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[0, 1], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'aridity_ratio']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[0, 2], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'runoff_ratio']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[0, 3], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_5']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[1, 0], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bf_magni']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[1, 1], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'a_q']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[1, 2], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'b_q']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[1, 3], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile10']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[2, 0], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_quantile90']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[2, 1], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'fdc_slope']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[2, 2], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_1']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[2, 3], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_2']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[3, 0], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'tau_roques']).set(xlabel = None, xticks=[])
sns.boxplot(ax=axes[3, 1], x='hydro_sig', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'bfi_90']).set(xlabel = None, xticks=[])

axes[3, 2].axis("off")
axes[3, 3].axis("off")










## En vrac - ne fonctionne pas toujours



# ## Boxplot of the hydrological signatures from their main hydrogeologic type perspective
# my_palette = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '9467bd', '8c564b', 'e377c2', '7f7f7f', 'bcbd22', '17becf']
# sns_palette_default = [(0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
#                     (1.0, 0.4980392156862745, 0.054901960784313725),
#                     (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
#                     (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
#                     (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
#                     (0.5490196078431373, 0.33725490196078434, 0.29411764705882354),
#                     (0.8901960784313725, 0.4666666666666667, 0.7607843137254902),
#                     (0.4980392156862745, 0.4980392156862745, 0.4980392156862745),
#                     (0.7372549019607844, 0.7411764705882353, 0.13333333333333333),
#                     (0.09019607843137255, 0.7450980392156863, 0.8117647058823529)]



sns.relplot(x='value', y='value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'a_q'])

watershed_code = shp_watersheds.loc[:,'code']

sns.relplot(x='XL93', y='YL93', hue = 'value', data=my_df.loc[my_df.loc[:, 'hydro_sig'] == 'q_mean'])
for i in watershed_code:
    watershed_i = shp_watersheds.loc[shp_watersheds.loc[:,'code']==i]
    plt.text(watershed_i.XL93, watershed_i.YL93, i, horizontalalignment='left', size='medium', color='black')

plt.show()

sns.relplot(x="XL93", y="YL93", hue="S_km2", data=my_df);

g = sns.FacetGrid(my_df, col="hydro_sig", col_wrap=2)
g.map(sns.scatterplot(x="XL93", y="YL93", hue="value", data = my_df), "XL93", "YL93")


def hydro_sig_plot(hydro_sig):
    df = my_df.loc[my_df.loc[:, 'hydro_sig'] == hydro_sig]
    sns.relplot(x="XL93", y="YL93", hue="value", data = df);

hydro_sig_name = ['q_mean', 'aridity_ratio', 'runoff_ratio']
g = sns.FacetGrid(my_df, col="hydro_sig", col_wrap=2)
for i in hydro_sig_name:
    print(i)
    g.map(hydro_sig_plot(i), "XL93", "YL93")
    
    
sns.relplot(x="XL93", y="YL93", hue="value",
               col="hydro_sig", col_wrap=2,
               kind="scatter", data= my_df);



# # bf indices
# sns.relplot(x="q_mean", y="bfi", hue="maingeol_description", data=df_some_stations)
# sns.relplot(x="bfi", y="bf_magni", hue="maingeol_description", data=df_some_stations)

#   # recession indices
# sns.relplot(x="q_mean", y="tau_1", hue="maingeol_description", data=df_some_stations)
# sns.relplot(x="q_mean", y="tau_2", hue="maingeol_description", data=df_some_stations)
# sns.relplot(x="tau_1", y="tau_2", hue="maingeol_description", data=df_some_stations)



# all_watershed = shp_contour.plot(color='white', edgecolor='#f4a460')
# problematic_watershed = shp_contour[shp_contour.Code=="A7821010"]
# problematic_watershed.plot(ax=all_watershed, color='blue');