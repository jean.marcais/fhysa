import geopandas as gpd
import numpy as np
import pandas as pd


class GeologicProperties(object):
    def __init__(self, code=-1, maingeol_id=-1, maingeol_description=-1, age=-1, proportion=-1, maingeol_age=-1, maingeol_age_proportion=-1, ks=-1, theta_d=-1, k_glhymps = -1):
        self.code = code
        self.maingeol_id = maingeol_id
        self.maingeol_description = maingeol_description
        self.maingeol_proportion = proportion
        self.age = age  # [min, mean, max] age averaged from the 1/1000000e brgm map
        self.maingeol_age = maingeol_age
        self.maingeol_age_proportion = maingeol_age_proportion
        self.ks = ks # [cm/day]
        self.theta_d = theta_d
        self.k_glhymps = k_glhymps # [cm/day]

    def extract_main_geology_from_filename(self, geol_foldername, catchment_contour_shp):
        geol_shp = gpd.read_file(geol_foldername)
        self.extract_main_geology(geol_shp, catchment_contour_shp)


    def extract_main_geology(self, geol_shp):
        geol_shp['area'] = geol_shp.geometry.area / 1e6
        geol_shp['prop'] = geol_shp.area / sum(geol_shp.area)
        geol_shp = geol_shp.loc[geol_shp.prop == max(geol_shp.prop)]
        self.maingeol_proportion = geol_shp.prop.values[0]
        self.maingeol_description = geol_shp.Type.values[0]
        # self.maingeol_id = geol_shp.index.values[0]
        self.maingeol_id = geol_shp.loc[:,'index'].values[0]
        

    def extract_average_age_geology(self, geol_filename):
        geol_shp = gpd.read_file(geol_filename)
        
        geol_shp['area'] = geol_shp.geometry.area / 1e6
        geol_shp['prop'] = geol_shp.area / sum(geol_shp.area)
        age_min = sum(geol_shp.prop*geol_shp.AGE_SUP)
        age_max = sum(geol_shp.prop*geol_shp.AGE_INF)
        age_mean = sum([age_max, age_min])/2
        self.age = [age_min, age_mean, age_max]

        self.maingeol_age_proportion = max(geol_shp.prop)
        geol_shp = geol_shp.loc[geol_shp.prop == max(geol_shp.prop)]
        maingeol_age_min = geol_shp.AGE_SUP.values[0]
        maingeol_age_max = geol_shp.AGE_INF.values[0]
        maingeol_age_mean = sum([maingeol_age_min, maingeol_age_max])/2
        self.maingeol_age = [maingeol_age_min, maingeol_age_mean, maingeol_age_max]
        
      
    def extract_mean_hydraulic_property(self, hydraulic_property_map):
        values, counts = np.unique(hydraulic_property_map[0], return_counts=True)
        my_np_array = np.array([values, counts]).transpose()
        ValAndCounts = pd.DataFrame(my_np_array, columns=['Values', 'Counts'])
        ValAndCounts = ValAndCounts[ValAndCounts.loc[:, 'Values'] != -1.7e+308] # remove pixels outside the studied area (value -1.7e+308)
        weight = ValAndCounts.Counts/(ValAndCounts.Counts.sum())
        average = (ValAndCounts.Values*weight).sum()
        return average 
    
    def compute_drainage_porosity(self, saturated_water_content_map, field_capacity_map):
        theta_s = self.extract_mean_hydraulic_property(saturated_water_content_map)
        theta_fc = self.extract_mean_hydraulic_property(field_capacity_map)
        self.theta_d = theta_s-theta_fc
    
    def compute_KS_mean(self, hydraulic_property_map):
        KS_mean_log10 = self.extract_mean_hydraulic_property(hydraulic_property_map)
        self.ks = pow(10,KS_mean_log10)
        
    def compute_hydraulic_properties(self, ks_map, sat_map, fc_map):
        self.compute_KS_mean(ks_map)
        self.compute_drainage_porosity(sat_map, fc_map)
        
    
    def extract_K_GLHYMPS(self, glhymps_file):
        where_gum_k_0 = glhymps_file['GUM_K'].eq(0)
        log_k = glhymps_file['logK_Ferr_']
        log_k = log_k.where(~where_gum_k_0, np.nan)
        k = pow(10,log_k/100) # k [m2] permeability
        K = k*999.97*9.8/(1e-3)
        K = np.nanmean(K)
        # k = np.nanmean(k)
        # K = k*999.97*9.8/(1e-3) # K [m/s] hydraulic conductivity
        K = K*1e2*24*3600
        self.k_glhymps = K
        
        
       
    

## From original BDLisa and BRGM files using clip function        

    # def extract_main_geology(self, geol_shp, catchment_contour_shp):
    #     geol_shp = gpd.clip(geol_shp, catchment_contour_shp)
    #     geol_shp['area'] = geol_shp.geometry.area / 1e6
    #     geol_shp['prop'] = geol_shp.area / sum(geol_shp.area)
    #     geol_shp = geol_shp.loc[geol_shp.prop == max(geol_shp.prop)]
    #     self.maingeol_proportion = geol_shp.prop.values[0]
    #     self.maingeol_description = geol_shp.Type.values[0]
    #     self.maingeol_id = geol_shp.index.values[0]

    # def extract_average_age_geology(self, geol_foldername, catchment_contour_shp):
    #     geol_shp = gpd.read_file(geol_foldername)
        
    #     try:
    #         geol_shp = gpd.clip(geol_shp, catchment_contour_shp)
    #     except:
    #         geol_shp = gpd.overlay(geol_shp, catchment_contour_shp)
        
    #     geol_shp['area'] = geol_shp.geometry.area / 1e6
    #     geol_shp['prop'] = geol_shp.area / sum(geol_shp.area)
    #     age_min = sum(geol_shp.prop*geol_shp.AGE_SUP)
    #     age_max = sum(geol_shp.prop*geol_shp.AGE_INF)
    #     age_mean = sum([age_max, age_min])/2
    #     self.age = [age_min, age_mean, age_max]

    #     self.maingeol_age_proportion = max(geol_shp.prop)
    #     geol_shp = geol_shp.loc[geol_shp.prop == max(geol_shp.prop)]
    #     maingeol_age_min = geol_shp.AGE_SUP.values[0]
    #     maingeol_age_max = geol_shp.AGE_INF.values[0]
    #     maingeol_age_mean = sum([maingeol_age_min, maingeol_age_max])/2
    #     self.maingeol_age = [maingeol_age_min, maingeol_age_mean, maingeol_age_max]
    