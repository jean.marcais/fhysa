# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 15:28:56 2021

@author: laura.lindeperg
"""

import rioxarray as rxr
import geopandas as gpd
import pandas as pd


# **************************** Data *****************************

# Watersheds
shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/616_Catchments.shp'
shp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'

shp_watersheds = gpd.read_file(shp_watersheds_path)

# Geomorphologic data

## ZHp
ZHp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/ZHp/'


## BDTopage
topage_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDTopage/'

# ## BDCarthage
# carthage_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDCarthage/'

## BDAlti 25m
bdalti_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDAlti_25m/'

## Slope
slope_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/Slope/'

## DTB
DTB_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/DTB/'


# List of the stations'codes
watershed_code = shp_watersheds.loc[:,'Code']
# Get a sample of them for test
code_for_test = watershed_code.loc[0:3]



# *************************** Extract indicators ************************************


from GeomorphologicProperties import GeomorphologicProperties
geomorpho_indicators = pd.DataFrame()
# for i in code_for_test:
for i in watershed_code:
    watershed_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    ZHp_i = rxr.open_rasterio(ZHp_foldername+i+'_ZHp.tif')
    hydro_sections_Topage_i = gpd.read_file(topage_foldername+i+'_BDTopage.shp')
    # hydro_sections_Carthage_i = gpd.read_file(carthage_foldername+i+'_BDCarthage.shp')
    # bdalti_i = rxr.open_rasterio(bdalti_foldername+i+'_BDAlti_25m.tif')
    # slope_i = rxr.open_rasterio(slope_foldername+i+'_slope_25m.tif')
    bdalti_path_i = bdalti_foldername+i+'_BDAlti_25m.tif'
    slope_path_i = slope_foldername+i+'_slope_25m.tif'
    DTB_path_i = DTB_foldername+i+'_DTB_250m.tif'
    
    geomorpho_i = GeomorphologicProperties(i)
    # geomorpho_i.compute_ZHp_ratio(ZHp_i)
    geomorpho_i.extract_geomorphologic_properties(watershed_contour_i, ZHp_i, hydro_sections_Topage_i, bdalti_path_i, slope_path_i, DTB_path_i)
    # geomorpho_i.extract_mean_elevation(bdalti_i)
    # geomorpho_i.extract_median_slope(slope_i)


    geomorpho_indicators = geomorpho_indicators.append(geomorpho_i.__dict__, ignore_index=True)


# And save it
# geomorpho_indicators.to_csv('616_stations_geomorpho_df.csv', index=False)




# ************************** Merge with extra geomorpho data **************************

delta_v_path = 'DeltaV_qualitycheck.csv'
delta_v = pd.read_csv(delta_v_path)

delta_v = delta_v[['Code', 'DeltaV', 'alpha']]
delta_v = delta_v.rename(columns = {'Code':'code'})


geomorpho_indicators = pd.read_csv('616_stations_geomorpho_df.csv')

geomorpho_indicators = pd.merge(geomorpho_indicators, delta_v, on = 'code') 

# Save
geomorpho_indicators.to_csv('616_stations_8geomorpho_df.csv', index=False)
