# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 12:34:40 2021

@author: laura.lindeperg
"""

import geopandas as gpd
import pandas as pd
import seaborn as sns
from Watershed import Watershed
import pickle


computer = 'laura'
# **************************** Data *****************************
if(computer == 'laura'):
    # Banque Hydro
    banquehydro_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Export2020/'
    # SAFRAN
    safran_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/SAFRAN/'
    # GEOL
    BDLisa_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/BDLisa/'
    BRGM_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/BRGM/'
    # Watersheds
    shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/complete_df_wrong_geometries.shp'
    shp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'
elif(computer== 'jean'):
    # Banque Hydro
    banquehydro_foldername = '/home/jean.marcais/Donnees/BanqueHydro/'  #'/media/jean.marcais/My Passport/DonneesLaura/BanqueHydro/Export2020/'
    # SAFRAN
    safran_foldername = '/home/jean.marcais/Donnees/DonneesLaura/Watersheds/SAFRAN/' #'/media/jean.marcais/My Passport/DonneesLaura/SAFRAN/daily/'
    # GEOL
    BDLisa_filepath = '/home/jean.marcais/Donnees/DonneesLaura/Watersheds/GEOL/BDLisa/'
    BRGM_filepath = '/home/jean.marcais/Donnees/DonneesLaura/Watersheds/GEOL/BRGM/'
    # Watersheds
    shp_watersheds_path = '/home/jean.marcais/Donnees/DonneesLaura/Watersheds/complete_df_wrong_geometries.shp'
    shp_foldername = '/home/jean.marcais/Donnees/DonneesLaura/Watersheds/GEOMETRY/'


shp_watersheds = gpd.read_file(shp_watersheds_path)

# List of the stations'codes
watershed_code = shp_watersheds.loc[:,'Code']
# Get a sample of them for test
code_for_test = watershed_code.loc[0:1]
# code_for_test = ['V1015810']
# code_for_test = ['V2814020', 'V5045020', 'H4033010', 'S4214010']
# code_for_test = ['E6456510', 'A7642010']

# *************************** Extract watersheds' properties ************************************

# problematic_watersheds = pd.DataFrame()
studied_watersheds = pd.DataFrame()

# for i in code_for_test:
for i in watershed_code:
    # Get the station's name
    watershed_stations_i = shp_watersheds[shp_watersheds.loc[:, 'Code'] == i]
    watershed_name_i =  watershed_stations_i.loc[:, 'Nom'].values[0]
    
    # Create waterhed object and extract its properties
    watershed_i = Watershed(i, watershed_name_i)
    shpfile_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    watershed_i.contour = shpfile_contour_i
    
    watershed_i.extract_banquehydro_discharge_timeseries(banquehydro_foldername) 
    safran_timeseries_i = pd.read_csv(safran_foldername+i+'_safran_timeseries.csv')
    watershed_i.hydro_climatic_fluxes.safran_timeseries = safran_timeseries_i
    watershed_i.extract_hydrological_signatures(adimensioned='no')

    # watershed_i.extract_geologic_properties_from_filename(BDLisa_filepath+i+'_BDLisa.shp')
    # watershed_i.geologic_properties.extract_average_age_geology(BRGM_filepath+i+'_BRGM.shp')
    
    # Create a dictionnary and fill the dataframe
    watershed_dict = watershed_i.to_dict()
    # problematic_watersheds = problematic_watersheds.append(watershed_dict, ignore_index=True)
    studied_watersheds = studied_watersheds.append(watershed_dict, ignore_index=True)
    
    # save object using pickle
    # filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/watershed_'+i
    filename_i = 'C:/Users/laura.lindeperg/Documents/INRAE_2021/CODE/fhysa/Object_watershed/Dim/watershed_dim_'+i
    outfile_i = open(filename_i, 'wb')
    pickle.dump(watershed_i, outfile_i)
    outfile_i.close()
    

# And save it
# studied_watersheds.to_csv('616_stations_hydrosig_df.csv', index=False)

## Open stored watershed objects
# i = code_for_test[0]
# filename_i = 'watershed_'+i
# infile_i = open(filename_i, 'rb')
# my_watershed = pickle.load(infile_i)
# infile_i.close()







# *************************** A piece of code which aims at extracting mean values of Pnet (Ptot-ET0) from safran timeseries

shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/616_Catchments.shp'
shp_watersheds = gpd.read_file(shp_watersheds_path)

safran_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/SAFRAN/'

# List of the stations'codes
watershed_code = shp_watersheds.loc[:,'Code']
# Get a sample of them for test
code_for_test = watershed_code.loc[0:1]

values_of_Pnet_moy = pd.DataFrame()

# for i in code_for_test:
for i in watershed_code:

    safran_timeseries_i = pd.read_csv(safran_foldername+i+'_safran_timeseries.csv')
    P_net_i = safran_timeseries_i['Ptot']-safran_timeseries_i['ET0']
    P_net_moy_i = P_net_i.mean()
    
    new_row = {'code' : i, 'P_net_moy' : P_net_moy_i}
    values_of_Pnet_moy = values_of_Pnet_moy.append(new_row, ignore_index=True)

# And save it
# values_of_Pnet_moy.to_csv('616_stations_Pnet_moy_df.csv', index=False)


















# df_v0 = pd.read_csv('4_stations_df_v0.csv')
# df_shp = pd.read_csv('4_stations_df_shp_read_from_files.csv')
# df_brgm = pd.read_csv('4_stations_df_shpANDbrgm_read_from_files.csv')
# df_bdlisa = pd.read_csv('4_stations_df_shpANDbrgmANDbdlisa_read_from_files.csv')


# ## Few plots for test
# df_some_stations_path = '510_first_stations_df.csv'
# df_some_stations = pd.read_csv(df_some_stations_path)

# # bf indices
# sns.relplot(x="q_mean", y="bfi", hue="maingeol_description", data=df_some_stations)
# sns.relplot(x="bfi", y="bf_magni", hue="maingeol_description", data=df_some_stations)

#   # recession indices
# sns.relplot(x="q_mean", y="tau_1", hue="maingeol_description", data=df_some_stations)
# sns.relplot(x="q_mean", y="tau_2", hue="maingeol_description", data=df_some_stations)
# sns.relplot(x="tau_1", y="tau_2", hue="maingeol_description", data=df_some_stations)



# all_watershed = shp_contour.plot(color='white', edgecolor='#f4a460')
# problematic_watershed = shp_contour[shp_contour.Code=="A7821010"]
# problematic_watershed.plot(ax=all_watershed, color='blue');





# ## PREVIOUS VERSION

# # **************************** Data *****************************

# # Banque Hydro
# banquehydro_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Export2020/'
# # banquehydro_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Export2020/Stations/A1080330.csv'

# # SAFRAN
# # safran_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/SAFRAN/daily/'
# # safran_grid_shpfilename = './TestData/SAFRAN/maille_meteo_fr_pr93.shp'
# safran_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/SAFRAN/'

# # GEOL
# # BDLisa_shp = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BD_Lisa/RegionalHydrogeologyAnalysisMe/BD_Lisa_regionalhydrogeology.shp'
# BDLisa_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/BDLisa/'

# # BRGM_geol_map = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/CarteGeolBRGM/FR_vecteur/FR_vecteur/GEO001M_CART_FR_S_FGEOL_2154.shp'
# BRGM_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/BRGM/'

# # Watersheds
# # shp_stations_filepath = 'E:/DonneesLaura/BanqueHydro/Shapes/StationBHYDRO_L93.shp'
# # df_stations_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/StationsNonInfluenceesExplore2/Synthèse analyses/Synthèse_meta_selection_624.csv'
# # shp_contour_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Shapes/BassinsVersantsMetropole/BV_4207_stations.shp' 

# # df_stations = pd.read_csv(df_stations_filepath, sep = ';', encoding='latin-1')
# # shp_contour = gpd.read_file(shp_contour_filepath)

# shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/complete_df_wrong_geometries.shp'
# shp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'

# shp_watersheds = gpd.read_file(shp_watersheds_path)

# # List of the stations'codes
# watershed_code = shp_watersheds.loc[:,'Code']
# # Get a sample of them for test
# code_for_test = watershed_code.loc[0:3]
# # code_for_test = ['A1072010', 'A1080330']
# # code_for_test = ['A1072010', 'A1080330', 'A3472010', 'I0102010', 'J3413030']

# # # extract safran gpd
# # from HydroClimaticFluxes import HydroClimaticFluxes
# # HCF = HydroClimaticFluxes(code=-1)
# # # Ptot_gpd = HCF.extract_safran_variable(safran_foldername, 'Ptot')
# # ET0_gpd = HCF.extract_safran_variable(safran_foldername, 'ET0')
# # Tair_gpd = HCF.extract_safran_variable(safran_foldername, 'Tair')
# # Snow_gpd = HCF.extract_safran_variable(safran_foldername, 'Snow')
# # Rain_gpd = HCF.extract_safran_variable(safran_foldername, 'Rain')

# # *************************** Extract watersheds' properties ************************************


# studied_watersheds = pd.DataFrame()
# for i in code_for_test:
# #for i in watershed_code:
#     # Get the station's name
#     watershed_stations_i = shp_watersheds[shp_watersheds.loc[:, 'Code'] == i]
#     watershed_name_i =  watershed_stations_i.loc[:, 'Nom'].values[0]
    
#     # Create waterhed object and extract its properties
#     watershed_i = Watershed(i, watershed_name_i)
#     shpfile_contour_i = gpd.read_file(shp_foldername+i+'.shp')
#     watershed_i.contour = shpfile_contour_i
#     # watershed_i.extract_watershed_contour_from_filename(shp_contour_filepath, 'Code')
#     watershed_i.extract_banquehydro_discharge_timeseries(banquehydro_foldername)
    
#     # watershed_i.extract_SAFRAN_forcings(safran_foldername, safran_grid_shpfilename)
#     # watershed_i.hydro_climatic_fluxes.intersect_safran_gpd_and_contour(Ptot_gpd, watershed_i.contour,'Ptot')
#     # watershed_i.hydro_climatic_fluxes.intersect_safran_gpd_and_contour(ET0_gpd, watershed_i.contour, 'ET0')
#     # watershed_i.hydro_climatic_fluxes.intersect_safran_gpd_and_contour(Tair_gpd, watershed_i.contour, 'Tair')
#     # watershed_i.hydro_climatic_fluxes.intersect_safran_gpd_and_contour(Snow_gpd, watershed_i.contour, 'Snow')
#     # watershed_i.hydro_climatic_fluxes.intersect_safran_gpd_and_contour(Rain_gpd, watershed_i.contour, 'Rain')
#     # watershed_i.hydro_climatic_fluxes.safran_timeseries['Ptot'] = watershed_i.hydro_climatic_fluxes.safran_timeseries['Snow'] + watershed_i.hydro_climatic_fluxes.safran_timeseries['Rain']
    
#     safran_timeseries_i = pd.read_csv(safran_foldername+i+'_safran_timeseries.csv')
#     watershed_i.hydro_climatic_fluxes.safran_timeseries = safran_timeseries_i
    
#     watershed_i.extract_hydrological_signatures()
#     # watershed_i.extract_geologic_properties_from_filename(BDLisa_shp)
#     watershed_i.extract_geologic_properties_from_filename(BDLisa_filepath+i+'_BDLisa.shp')
#     # watershed_i.geologic_properties.extract_average_age_geology(BRGM_geol_map, watershed_i.contour)
#     watershed_i.geologic_properties.extract_average_age_geology(BRGM_filepath+i+'_BRGM.shp')
    
#     # Create a dictionnary and fill the dataframe
#     watershed_dict = watershed_i.to_dict()
#     studied_watersheds = studied_watersheds.append(watershed_dict, ignore_index=True)


