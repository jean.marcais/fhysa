# -*- coding: utf-8 -*-
"""
Created on Wed Mar 24 09:21:44 2021

@author: laura.lindeperg
"""

import geopandas as gpd
import pandas as pd
from shapely.geometry import mapping
import rioxarray as rxr


# **************************** Data *****************************


## Watersheds

## ----- When creating complete waterheds file ----------
# shp_stations_filepath = 'E:/DonneesLaura/BanqueHydro/Shapes/StationBHYDRO_L93.shp'
df_stations_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/StationsNonInfluenceesExplore2/Synthèse analyses/Synthèse_meta_selection_624.csv'
shp_contour_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Shapes/BassinsVersantsMetropole/BV_4207_stations.shp'
shp_BV_IV_path ='C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/BV_InvalidesValides/BVsInvalidesValides.shp'

df_stations = pd.read_csv(df_stations_filepath, sep = ';', encoding='latin-1')
shp_contour = gpd.read_file(shp_contour_filepath)
shp_BV_IV = gpd.read_file(shp_BV_IV_path)

# shp_BV_VV = gpd.read_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/BV_InvalidesValides/BVsValidesValides.shp')
# -----------------------

shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/616_Catchments.shp'
shp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'

shp_watersheds = gpd.read_file(shp_watersheds_path)




## GEOMORPHO


# Depth to Bedrock
depth_to_bedrock_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/SoilGrids/BDTICM_M_250m_France.tif'
depth_to_bedrock = rxr.open_rasterio(depth_to_bedrock_path).squeeze()

# BDTopage
hydro_sections_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/TronconHydrographique_FXX-shp/TronconHydrographique_FXX.shp'
hydro_sections = gpd.read_file(hydro_sections_path)

# BDCarthage - v.2017
hydro_sections_Carthage_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BDCarthage/TronconHydrograElt_FXX-shp/TronconHydrograElt_FXX.shp'
hydro_sections_Carthage = gpd.read_file(hydro_sections_Carthage_path)

# ZHp (raster)
ZHp_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/ZonesHumidesPotentielles/mph-fr.tif'
ZHp_map = rxr.open_rasterio(ZHp_map_path).squeeze()

# BDAlti 25m
BDAlti_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BDAlti_25m/France_BDAlti_25m.tif'
BDAlti_map = rxr.open_rasterio(BDAlti_map_path).squeeze()


## GEOL

# GLHYMPS
GLHYMPS_shp_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/GLHYMPS/GLHYMPS_FRcropped.shp'
GLHYMPS_shp = gpd.read_file(GLHYMPS_shp_path)

# BDLisa
BDLisa_shp_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BD_Lisa/RegionalHydrogeologyAnalysisMe/BD_Lisa_regionalhydrogeology.shp'
BDLisa_shp = gpd.read_file(BDLisa_shp_path)

# BRGM
BRGM_geol_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/CarteGeolBRGM/FR_vecteur/FR_vecteur/GEO001M_CART_FR_S_FGEOL_2154.shp'
BRGM_shp = gpd.read_file(BRGM_geol_path)

# SATURATED - Water retention of topsoil: saturated water content - cm3/cm3 no units
SAT_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/ths_fao_octop.tif'
SAT_map = rxr.open_rasterio(SAT_map_path).squeeze()

# KS - Saturated hydraulic conductivity of topsoil - log10(cm/day)
KS_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/ks_fao_octop.tif'
KS_map = rxr.open_rasterio(KS_map_path).squeeze()

# FC - Water retention of topsoil:  water content at  field capacity - cm3/cm3 no units
FC_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/fc_fao.tif'
FC_map = rxr.open_rasterio(FC_map_path).squeeze()


## HYDROCLIMATIC

# Banque Hydro
banquehydro_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Export2020/'

# SAFRAN
safran_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/SAFRAN/daily/'
# safran_grid_shpfilename = './TestData/SAFRAN/maille_meteo_fr_pr93.shp'


# extract safran gpd
from HydroClimaticFluxes import HydroClimaticFluxes
HCF = HydroClimaticFluxes(code=-1)
# Ptot_gpd = HCF.extract_safran_variable(safran_foldername, 'Ptot')
ET0_gpd = HCF.extract_safran_variable(safran_foldername, 'ET0')
Tair_gpd = HCF.extract_safran_variable(safran_foldername, 'Tair')
Snow_gpd = HCF.extract_safran_variable(safran_foldername, 'Snow')
Rain_gpd = HCF.extract_safran_variable(safran_foldername, 'Rain')



# **************************** Create my own Data *****************************

# Select only Code, Name, XL93 and YL93 columns
my_df_stations = df_stations.loc[:, 'Code':'YL93' ]

# Track duplicates
duplicated_stations = my_df_stations.duplicated(subset = ['Code']) # -> True for the second occurrence in Code column
index_duplicated = duplicated_stations[duplicated_stations[:,] == True]
stations_duplicated = my_df_stations.iloc[index_duplicated.index[0]]

# And remove them (the second occurrence is removed)
no_duplicated = my_df_stations.drop(index_duplicated.index[0])

# Merge with shp file to create a complete df
# shp_watersheds = my_df_stations.merge(shp_contour, on = 'Code', how = 'outer')
shp_watersheds = no_duplicated.merge(shp_contour, on = 'Code', how = 'outer')
shp_watersheds = gpd.GeoDataFrame(shp_watersheds)
no_geometry = shp_watersheds[shp_watersheds['geometry'] == None]

# Only keep the studied watersheds that is to say those in the 600 stations df
studied_watersheds = shp_watersheds.loc[shp_watersheds.loc[:,'Code'].isin(no_duplicated.loc[:,'Code'])]
# And remove thos with no geometry
complete_watersheds = studied_watersheds[studied_watersheds['geometry'] != None]

## Update of the complete_watersheds file due to missing bv in BH -> 'K9321810'
watershed_code = complete_watersheds.loc[:,'Code']
no_BHfile = pd.DataFrame()
for i in watershed_code:
    try:
        BHfile = pd.read_table(banquehydro_foldername+i+'_qj_hydro2.txt', encoding='latin-1')
    except:
        no_BHfile = no_BHfile.append({'Code': i}, ignore_index = True)

complete_watersheds = complete_watersheds[complete_watersheds.loc[:,'Code'].isin(no_BHfile.loc[:,'Code']) == False]

# And save file
# complete_watersheds.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/complete_df_wrong_geometries.shp')

# complete_watersheds_noElle = complete_watersheds.drop(complete_watersheds.loc[complete_watersheds.loc[:, 'Code'] == 'J4742020'].index[0])


## Dealing with BV_IV
df_BV_IV = no_duplicated.loc[no_duplicated.loc[:,'Code'].isin(shp_BV_IV.loc[:,'Code'])]
shp_BV_IV = shp_BV_IV.loc[:, ['Code', 'S_km2', 'dt_pstn', 'geometry']]
bv_IV = df_BV_IV.merge(shp_BV_IV, on = 'Code', how = 'outer')
bv_IV = gpd.GeoDataFrame(bv_IV)

# bv_IV.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/BV_IV.shp')

## Replacing problematic watersheds'geometry in final df -> doesn't work for the moment
# bv_IV_studied = complete_watersheds.loc[complete_watersheds.loc[:,'Code'].isin(bv_IV.loc[:,'Code'])]
# new_complete = complete_watersheds.update(bv_IV_studied)
# index_BV_Inv = complete_watersheds.loc[complete_watersheds.loc[:,'Code'].isin(bv_IV.loc[:,'Code'])].index
# index_BV_Inv = pd.DataFrame(index_BV_Inv)
# new_geometries = bv_IV.loc[:, ['Code', 'geometry']]
# for i in index_BV_Inv:
#     print(i)
#     geometry = bv_IV.loc[bv_IV.loc[:, 'Code'] == complete_watersheds.loc[i, 'Code']]
#     print(geometry)
#     # print(geometry.__dict__)
#     complete_watersheds = complete_watersheds.replace({'geometry' : i }, geometry.loc[:,'geometry'])
#     print(complete_watersheds.loc[i, 'geometry'])



# *************************** Create watersheds' shapefiles and rasters ************************************

# List of the stations'codes
watershed_code = shp_watersheds.loc[:,'Code']
# watershed_code = complete_watersheds.loc[:,'Code']
# watershed_code_SAFRAN = complete_watersheds_noElle.loc[:,'Code']
# watershed_code_IV = shp_BV_IV.loc[shp_BV_IV.loc[:,'Code'].isin(watershed_code)].loc[:, 'Code']

# Get a sample of them for test
code_for_test = watershed_code.loc[614:]
# code_for_test = ['K9341810']
# code_for_test = ['J4742020']


# GLHYMPS

for i in code_for_test:
# for i in watershed_code:
    
    ## Get shp
    shp_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    
    # Hydro sections - BDCarthage v.2017
    try:
        shpfile_GLHYMPS = gpd.clip(GLHYMPS_shp, shp_contour_i)
    except:
        shpfile_GLHYMPS = gpd.overlay(GLHYMPS_shp, shp_contour_i)
    
    shpfile_GLHYMPS.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/GLHYMPS/'+i+'_GLHYMPS.shp')


## GEOMORPHOLOGY

# BD Carthage v.2017 and Depth to Bedrock

for i in code_for_test:
# for i in watershed_code:
    
    ## Get shp
    shp_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    
    # Hydro sections - BDCarthage v.2017
    try:
        shpfile_BDCarthage = gpd.clip(hydro_sections_Carthage, shp_contour_i)
    except:
        shpfile_BDCarthage = gpd.overlay(hydro_sections_Carthage, shp_contour_i)
        
    shpfile_BDCarthage.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDCarthage/'+i+'_BDCarthage.shp')

    ## Depth to Bedrock
    
    raster_DTB = depth_to_bedrock.rio.clip(shp_contour_i.geometry.apply(mapping), shp_contour_i.crs)
    raster_DTB.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/DTB/'+i+'_DTB_250m.tif')


# BDAlti 25m

for i in code_for_test:
# for i in watershed_code:
    
     ## Get shp
    shp_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    
    
    raster_BDAlti = BDAlti_map.rio.clip(shp_contour_i.geometry.apply(mapping), shp_contour_i.crs)
    raster_BDAlti.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/TOPO/BDAlti_25m/'+i+'_BDAlti_25m.tif')


## Soil hydraulic properties
 
# for i in code_for_test:
for i in watershed_code:
    
     ## Get shp
    shp_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    
    ## Get hydraulic properties data within the watershed and save them in file
    # SATURATED - Water retention of topsoil: saturated water content - cm3/cm3 no units
    raster_SAT = SAT_map.rio.clip(shp_contour_i.geometry.apply(mapping), shp_contour_i.crs)  
    raster_SAT.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/JRC-ESDAC/SAT/'+i+'_SAT.tif')

    # KS - Saturated hydraulic conductivity of topsoil - log10(cm/day)
    raster_KS = KS_map.rio.clip(shp_contour_i.geometry.apply(mapping), shp_contour_i.crs)  
    raster_KS.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/JRC-ESDAC/KS/'+i+'_KS.tif')
    
    # FC - Water retention of topsoil:  water content at  field capacity - cm3/cm3 no units
    raster_FC = FC_map.rio.clip(shp_contour_i.geometry.apply(mapping), shp_contour_i.crs)
    raster_FC.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/JRC-ESDAC/FC/'+i+'_FC.tif')


# BD Topage

# for i in code_for_test:
for i in watershed_code:
    
     ## Get shp
    shp_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    
    ## Get geomorphologic data within the watershed and save them in file
    # Hydro sections - BDTopage
    try:
        shpfile_BDTopage = gpd.clip(hydro_sections, shp_contour_i)
    except:
        shpfile_BDTopage = gpd.overlay(hydro_sections, shp_contour_i)
        
    shpfile_BDTopage.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDTopage/'+i+'_BDTopage.shp')
    
    
    # Hydro sections - BDCarthage
    try:
        shpfile_BDCarthage = gpd.clip(hydro_sections_Carthage, shp_contour_i)
    except:
        shpfile_BDCarthage = gpd.overlay(hydro_sections_Carthage, shp_contour_i)
        
    shpfile_BDCarthage.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDCarthage/'+i+'_BDCarthage.shp')



# for i in code_for_test:
for i in watershed_code:
    
    ## Get shp
    shp_contour_i = gpd.read_file(shp_foldername+i+'.shp')

    ## Get geomorphologic data within the watershed and save them in file
    # ZHp

    raster_ZHp = ZHp_map.rio.clip(shp_contour_i.geometry.apply(mapping), shp_contour_i.crs)
        
    raster_ZHp.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/ZHp/'+i+'_ZHp.tif')
    
    

## GEOL

# for i in code_for_test:
for i in watershed_code:
    
    ## Get shp
    shpfile_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    # shpfile_contour_i = shp_contour[shp_contour.loc[:, 'Code'] == i]
    # And save it in a file (identifier = its code)
    # shpfile_contour_i.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'+i+'.shp')
    
    ## Get watershed's geologic properties and save them in file
    
    # BDLisa
    try:
        shpfile_BDLisa = gpd.clip(BDLisa_shp, shpfile_contour_i)
    except:
        shpfile_BDLisa = gpd.overlay(BDLisa_shp, shpfile_contour_i)
        
    shpfile_BDLisa.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/BDLisa/'+i+'_BDLisa.shp')
    
    # BRGM geol
    try:
        shpfile_BRGM = gpd.clip(BRGM_shp, shpfile_contour_i)
    except:
        shpfile_BRGM = gpd.overlay(BRGM_shp, shpfile_contour_i)
        
    shpfile_BRGM.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/BRGM/'+i+'_BRGM.shp')
    

## SAFRAN csv
    
for i in code_for_test:
# for i in watershed_code_IV:
    
    ## Get shp
    shpfile_contour_i = complete_watersheds[complete_watersheds.loc[:, 'Code'] == i]
    # shpfile_contour_i = bv_IV[bv_IV.loc[:, 'Code'] == i]
    # And save it in a file (identifier = its code)
    # shpfile_contour_i.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'+i+'.shp')
    
    HCF = HydroClimaticFluxes(code = i)
    HCF.intersect_safran_gpd_and_contour(ET0_gpd, shpfile_contour_i, 'ET0')
    HCF.intersect_safran_gpd_and_contour(Tair_gpd, shpfile_contour_i, 'Tair')
    HCF.intersect_safran_gpd_and_contour(Snow_gpd, shpfile_contour_i, 'Snow')
    HCF.intersect_safran_gpd_and_contour(Rain_gpd, shpfile_contour_i, 'Rain')
    HCF.safran_timeseries['Ptot'] = HCF.safran_timeseries['Snow'] + HCF.safran_timeseries['Rain']     
        
    # HCF.safran_timeseries.to_csv('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/SAFRAN/'+i+'_safran_timeseries.csv', index=False)  
        
    


# Test
safran_test = pd.read_csv('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/SAFRAN/A1072010_safran_timeseries.csv', encoding='latin-1')




## Extract 400 stations files and filter size

shp_contour_filepath = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BanqueHydro/Shapes/BassinsVersantsMetropole/BV_4207_stations.shp'
shp_contour = gpd.read_file(shp_contour_filepath)

foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'


watershed_code = shp_contour.loc[:,'Code']
code_for_test = watershed_code.loc[0:70]
unexisting_files = pd.DataFrame()
for i in watershed_code:
# for i in code_for_test:
    try:
        file = gpd.read_file(foldername+i+'.shp')
    except:
        unexisting_files = unexisting_files.append({'Code': i}, ignore_index = True)


    
    













