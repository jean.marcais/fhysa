# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 15:35:20 2021

@author: laura.lindeperg
"""

import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import mapping
import rioxarray as rxr
import xarray as xr
import geopandas as gpd
import pandas as pd
from pysheds.grid import Grid

# **************************** Data *****************************

# Watersheds
shp_watersheds_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/complete_df_wrong_geometries.shp'
shp_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/'

shp_watersheds = gpd.read_file(shp_watersheds_path)

rrse_241_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/STATIONS/Stations241_RRSE.csv'
rrse_241 = pd.read_csv(rrse_241_path)

# List of the stations'codes
watershed_code = shp_watersheds.loc[:,'Code']
# Get a sample of them for test
code_for_test = watershed_code.loc[0:3]

rrse_241 = rrse_241.rename(columns = {'RRSE':'Code'})
rrse_241 = rrse_241.loc[:, 'Code']

shared_codes = rrse_241.loc[rrse_241.isin(watershed_code) == True]


bv_for_test_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMETRY/J4742020.shp'
bv_for_test = gpd.read_file(bv_for_test_path)






#----------GEOMORPHO


## Depth to Bedrock
depth_to_bedrock_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/DTB/V1020010_DTB_250m.tif'
depth_to_bedrock = rxr.open_rasterio(depth_to_bedrock_path, masked = True).squeeze()


## BDAlti 25m
BDAlti_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/BDAlti_25m/J4742020_BDAlti_25m.tif'
BDAlti_test = rxr.open_rasterio(BDAlti_path, masked=True).squeeze()


## Slope
slope_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOMORPHO/Slope/J4742020_slope_25m.tif'
slope = rxr.open_rasterio(slope_path).squeeze()


## BDTopage

# Tronçons hydro
hydro_sections_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/TronconHydrographique_FXX-shp/TronconHydrographique_FXX.shp'
hydro_sections = gpd.read_file(hydro_sections_path)

# Extraction d'un shp propre au bv test et enregistrement
troncons_hydro_clipped = gpd.clip(hydro_sections, bv_for_test)
troncons_hydro_clipped.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/TronconHydrographique_FXX-shp/bv_for_test_TronconHydro.shp')


troncon_hydro_test_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/TronconHydrographique_FXX-shp/bv_for_test_TronconHydro.shp'
troncon_hydro_test = gpd.read_file(troncon_hydro_test_path)

## BDCarthage

hydro_sections_Carthage_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/troncon-hydrographique/TRONCON_HYDROGRAPHIQUE.shp'
hydro_sections_Carthage = gpd.read_file(hydro_sections_Carthage_path)
# Extraction d'un shp propre au bv test et enregistrement
hydro_sections_Carthage_clipped = gpd.clip(hydro_sections_Carthage, bv_for_test)
hydro_sections_Carthage_clipped.to_file('C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/troncon-hydrographique/bv_for_test_TronconHydroCarthage.shp')


hydro_sections_Carthage_test_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Topage/troncon-hydrographique/bv_for_test_TronconHydroCarthage.shp'
hydro_sections_Carthage_test = gpd.read_file(hydro_sections_Carthage_test_path)


## ZHp

# Carte des Zones Humides (raster)
ZHp_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/ZonesHumidesPotentielles/mph-fr.tif'
ZHp_map = rxr.open_rasterio(ZHp_map_path).squeeze()

# extraction du raster propre au contour du bassin versant et enregistrement du raster obtenu
ZHp_clipped = ZHp_map.rio.clip(bv_for_test.geometry.apply(mapping), bv_for_test.crs)
ZHp_clipped.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/ZonesHumidesPotentielles/J4742020_ZHp.tif')

# Raster test
ZHp_test_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/ZonesHumidesPotentielles/J4742020_ZHp.tif'
ZHp_test = rxr.open_rasterio(ZHp_test_path).squeeze()  # masked=True pour masquer les noData



#------------GEOL

## GLHYMPS
GLHYMPS_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/GEOL/GLHYMPS/V1020010_GLHYMPS.shp' # V1020010
glhymps = gpd.read_file(GLHYMPS_path)

## Hydraulic conductivity of topsoil

# Raster file
KS_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/ks_fao_octop.tif'
KS_map = rxr.open_rasterio(KS_map_path).squeeze()

# Raster extraction from the watershed's geometry
KS_clipped = KS_map.rio.clip(bv_for_test.geometry.apply(mapping), bv_for_test.crs)
KS_clipped.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/J4742020_KS.tif')

# Raster test
KS_test_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/J4742020_KS.tif'
KS_test = rxr.open_rasterio(KS_test_path).squeeze() # don't use masked = True to obtain classes and counts to avoid creating classes for each Nan value... but use it to map and plot raster


# FC - Water retention of topsoil:  water content at  field capacity
FC_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/fc_fao.tif'
FC_map = rxr.open_rasterio(KS_map_path).squeeze()

FC_clipped = FC_map.rio.clip(bv_for_test.geometry.apply(mapping), bv_for_test.crs)
FC_clipped.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/J4742020_FC.tif')

# Raster test
FC_test_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/FS_water_2016_07/J4742020_FC.tif'
FC_test = rxr.open_rasterio(FC_test_path).squeeze()





# *************************** Extract indicators ************************************



def extract_GUMinfo_GLHYMPS(glhymps_file):
    gum_k = glhymps_file['GUM_K']
    class_proportion = gum_k.value_counts(normalize = True)
    from_GUM = class_proportion.loc[2]
    try:
       no_data = class_proportion.loc[0]
    except:
       no_data = 0   
    
    
def extract_info_GLHYMPS(glhymps_file):
    
    
    
def extract_K_GLHYMPS(glhymps_file):
    where_gum_k_0 = glhymps_file['GUM_K'].eq(0)
    log_k = glhymps_file['logK_Ferr_']
    log_k = log_k.where(~where_gum_k_0, np.nan)
    k = pow(10,log_k/100)
    k = np.nanmean(k)
    K = k*999.97*9.8/(1e-3)
    K = K*1e2*24*3600
    
    
    

def extract_mean_DTB(self, DTB_map):
    import numpy.ma as ma
    mean_DTB = np.mean(ma.masked_values(DTB_map, DTB_map._FillValue)) # if masked = False when reading raster
    return mean_DTB


def extract_mean_DTB(self, DTB_map):
    mean_DTB = np.nanmean(DTB_map) # if masked = True when reading raster
    return mean_DTB


def extract_mean_elevation(self, topographic_property_map):
    import numpy.ma as ma
    mean_elevation = np.mean(ma.masked_values(topographic_property_map, topographic_property_map._FillValue))
    return mean_elevation
        

def extract_median_slope(topographic_property_map):
    import numpy.ma as ma
    slope = ma.masked_values(topographic_property_map, topographic_property_map._FillValue)
    slope = slope.filled(np.nan)
    median_slope = np.nanmedian(slope)
    return median_slope




topographic_property_map = BDAlti_test

def extract_mean_elevation(topographic_property_map):
    values, counts = np.unique(topographic_property_map, return_counts=True)
    my_np_array = np.array([values, counts]).transpose()
    ValAndCounts = pd.DataFrame(my_np_array, columns=['Values', 'Counts'])
    ValAndCounts = ValAndCounts[ValAndCounts.loc[:, 'Values'] != -99999] # remove pixels outside the studied area (value -99999)
    weight = ValAndCounts.Counts/(ValAndCounts.Counts.sum())
    average = (ValAndCounts.Values*weight).sum()
    return average

import numpy.ma as ma
mean_elevation = np.mean(ma.masked_values(topographic_property_map, -99999.))








# BDAlti 25m
BDAlti_map_path = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BDAlti_25m/France_BDAlti_25m.tif'
BDAlti_map = rxr.open_rasterio(BDAlti_map_path).squeeze()

BDAlti_clipped = BDAlti_map.rio.clip_box(
                    minx=2.018e+05,
                    miny=6.772e+06,
                    maxx=2.342e+05,
                    maxy=6.809e+06,)

BDAlti_clipped.rio.to_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/BDAlti_25m/clipped.tif')

Alticlip = rxr.open_rasterio('C:/Users/laura.lindeperg/Documents/DonneesLaura/BDAlti_25m/clipped.tif').squeeze()



grid = Grid.from_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/BDAlti_25m/clipped.tif', data_name='dem')

grid.read_raster(data = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/BDAlti_25m/clipped.tif', data_name='dem', nodata=-99999.)
elevation = grid.dem

# Determine D8 flow directions from DEM
# ----------------------
# Fill depressions in DEM
grid.fill_depressions('dem', out_name='flooded_dem')
    
# Resolve flats in DEM
grid.resolve_flats('flooded_dem', out_name='inflated_dem')
    
# Specify directional mapping
dirmap = (64, 128, 1, 2, 4, 8, 16, 32)
    
# Compute flow directions
# -------------------------------------
grid.flowdir(data='inflated_dem', out_name='dir', dirmap=dirmap)
fdir = grid.dir


grid.cell_slopes(fdir = 'dir', dem = 'dem', out_name = 'slopes', nodata_in=0)
slope = grid.slopes












grid = Grid.from_raster('C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/TOPO/BDAlti_25m/J4742020_BDAlti_25m.tif', data_name='dem')

grid.read_raster(data = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/Watersheds/TOPO/BDAlti_25m/J4742020_BDAlti_25m.tif', data_name='dem', nodata=-99999.)


import numpy.ma as ma
crs = BDAlti_test.rio.crs
BDAlti_newtest = np.array(BDAlti_test)
masked = ma.masked_values(BDAlti_test, -99999.)
mask = np.isfinite(masked)
new = mask.filled(False)
new.rio.to_raster('test.tif')


# x = ma.array(BDAlti_test, mask=masked)
y= masked.filled(np.nan)
'+proj=lcc +lat_0=46.5 +lon_0=3 +lat_1=49 +lat_2=44 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'

grid.add_gridded_data(y, data_name = 'test', crs={'SRS' :'EPSG:2154'} )


# Determine D8 flow directions from DEM
# ----------------------
# Fill depressions in DEM
grid.fill_depressions('dem', out_name='flooded_dem', nodata_in=-99999.)

# Resolve flats in DEM
grid.resolve_flats('flooded_dem', out_name='inflated_dem', nodata_in=-99999.)

# Specify directional mapping
#N    NE    E    SE    S    SW    W    NW
dirmap = (64, 128, 1, 2, 4, 8, 16, 32)
    
# Compute flow directions
# -------------------------------------
grid.flowdir(data='inflated_dem', out_name='dir', dirmap=dirmap, nodata_in = -99999.)
fdir = grid.dir  

dirmap = (70, 100, 21, 20, 47, 82, 1, 2)

grid.flowdir(data='inflated_dem', out_name='dir_1', dirmap=dirmap)
fdir_1 = grid.dir_1 

grid.cell_slopes(fdir = 'dir', dem = 'dem', out_name = 'slopes', nodata_in=0)
slope = grid.slopes
median_slope = np.median(slope[np.isfinite(slope)])

grid.cell_slopes(fdir = 'dir', dem = 'dem', dirmap = dirmap, out_name = 'slopes_dirmap')
slope_dirmap = grid.slopes_dirmap
median_slope_dirmap = np.median(slope_dirmap[np.isfinite(slope_dirmap)])

grid.cell_slopes(fdir = 'dir_1', dem = 'dem', out_name = 'slopes_1')
slope_1 = grid.slopes_1
median_slope_1 = np.median(slope_1[np.isfinite(slope_1)])


grid.flowdir(data='inflated_dem', out_name='dir')
grid.flowdir(data='inflated_dem_1', out_name='dir_1', nodata_in= -99999)
grid.view('dir')

grid.cell_slopes(fdir = 'dir', dem = 'BDAlti_raster', out_name = 'slopes')
grid.cell_slopes(fdir = 'dir_1', dem = 'BDAlti_raster', out_name = 'slopes_1', nodata_in = 0)

grid.cell_area()
array_area = grid.area
dem = grid.BDAlti_raster
slope = grid.slopes
slope_1 = grid.slopes_1
median_slope = np.median(slope[np.isfinite(slope)])
median_slope_1 = np.median(slope[~np.isnan(slope)])
from numpy import nanmedian, NaN
median_slope_2 = nanmedian(slope)

fdir_1 = grid.dir_1

#N    NE    E    SE    S    SW    W    NW
dirmap = (70,  100,  10,   20,    44,   8,    16,  32)

# Access the flow direction grid
fdir = grid.dir

# Plot the flow direction grid
fig = plt.figure(figsize=(8,6))
plt.imshow(fdir, extent=grid.extent, cmap='viridis', zorder=1)
boundaries = ([0] + sorted(list(dirmap)))
plt.colorbar(boundaries= boundaries,
             values=sorted(dirmap))
plt.title('Flow direction grid')
plt.xlabel('Longitude')
plt.ylabel('Latitude')



# Plot the result
fig, ax = plt.subplots(figsize=(8,6))
plt.imshow(raster_slopes, cmap='bone', zorder=1,
           extent=grid.extent)
plt.colorbar(boundaries = [0, 1], label='Slopes (m - cells)')
plt.title('Slopes')
plt.xlabel('Longitude')
plt.ylabel('Latitude')






classesO, countsO = np.unique(BDAlti_test, return_counts=True)

# ZHp_test.rio.resolution()
# BV_W3315010.geometry.area


classes_ks, counts_ks = np.unique(KS_test[0], return_counts=True)
classes, counts = np.unique(KS_test[1], return_counts=True)

classes_fc, counts_fc = np.unique(FC_test[0], return_counts=True)


# Quelques tests

FC_test[0].plot.hist()
plt.show()

FC_test[0].plot()
plt.show()


KS_test[0].plot.hist()
plt.show()

KS_test[0].plot()
plt.show()



depth_to_bedrock.plot.hist()
plt.show()

f, ax = plt.subplots(figsize=(10, 4))
depth_to_bedrock.plot(ax=ax,
                 cmap='Greys')
ax.set(title="Final Clipped ZHp")
ax.set_axis_off()
plt.show()










def extract_mean_hydraulic_property(hydraulic_property_map):
    values, counts = np.unique(hydraulic_property_map[0], return_counts=True)
    my_np_array = np.array([values, counts]).transpose()
    ValAndCounts = pd.DataFrame(my_np_array, columns=['Values', 'Counts'])
    ValAndCounts = ValAndCounts[ValAndCounts.loc[:, 'Values'] != -1.7e+308] # remove pixels outside the studied area (value -1.7e+308)
    weight = ValAndCounts.Counts/(ValAndCounts.Counts.sum())
    average = (ValAndCounts.Values*weight).sum()
    return average

# def extract_mean_hydraulic_property(hydraulic_property_map):
#     values, counts = np.unique(hydraulic_property_map[0], return_counts=True)
#     ValAndCounts = pd.DataFrame()
#     ValAndCounts['Values'] = values
#     ValAndCounts['Counts'] = counts
#     ValAndCounts = ValAndCounts[ValAndCounts.loc[:, 'Values'] != -1.7e+308] # remove pixels outside the studied area (value -1.7e+308)
#     weight = ValAndCounts.Counts/(ValAndCounts.Counts.sum())
#     average = (ValAndCounts.Values*weight).sum()
#     return average


def compute_KS_mean(hydraulic_property_map):
    KS_mean_log10 = extract_mean_hydraulic_property(hydraulic_property_map)
    KS_mean = pow(10,KS_mean_log10)
    return KS_mean





def compute_order_1_ratio(hydro_sections):
    # Hydro sections - Total length
    total_length = hydro_sections.length.sum()

    # Keep order 1 only
    strahler_class_1 = hydro_sections[hydro_sections.loc[:,'GIGE_Sthr'] == 1]
    order_1_length = strahler_class_1.length.sum()
    
    order_1_ratio = order_1_length/total_length
    return order_1_ratio

def compute_intermittent_ratio(hydro_sections):
    # Hydro sections - Total length
    total_length = hydro_sections.length.sum()
    
    # Keep classes 1, 2 and 3 only (dry, ephemeral and intermittent)
    intermittent = hydro_sections[hydro_sections.loc[:,'Persistanc'].isin(['1','2','3'])]
    intermittent_length = intermittent.length.sum()
    
    intermittent_ratio = intermittent_length/total_length
    return intermittent_ratio
    
def compute_persistence_nodata_ratio(hydro_sections):
    # Hydro sections - Total length
    total_length = hydro_sections.length.sum()
    
    # Compute ratio of NoData
    no_data = hydro_sections[hydro_sections.loc[:,'Persistanc'] == '0'] # 0 is the NoData class
    no_data_ratio = no_data.length.sum()/total_length
    return no_data_ratio

def compute_drainage_density(contour, hydro_sections):
    # Hydro sections
    total_length = hydro_sections.length.sum()
    # Area
    surface_area = contour.geometry.area
    
    drainage_density = float(total_length/surface_area)
    return drainage_density

    
def compute_ZHp_ratio(ZHp):
    # ZHp_test.values.flatten() -> fonction "unique" s'en occupe si ce n'est pas 1D
    classes, counts = np.unique(ZHp, return_counts=True)
    my_np_array = np.array([classes, counts]).transpose()
    ZHp_ClassAndCounts = pd.DataFrame(my_np_array, columns=['Classes', 'Counts'])
    ZHp_ClassAndCounts = ZHp_ClassAndCounts[ZHp_ClassAndCounts.loc[:, 'Classes'] != 65535] # remove pixels outside the studied area (class 65535)
    total_classes = ZHp_ClassAndCounts.loc[:,'Counts'].sum()
    ZHp_classes = ZHp_ClassAndCounts[ZHp_ClassAndCounts.loc[:, 'Classes'].isin([2,3,51])]
    ZHp_classes = ZHp_classes.loc[:,'Counts'].sum()
    ZHp_ratio = ZHp_classes/total_classes
    return ZHp_ratio

geomorpho_indicators = pd.DataFrame()
for i in code_for_test:
    watershed_contour_i = gpd.read_file(shp_foldername+i+'.shp')
    ZHp_within_bv = ZHp_map.rio.clip(watershed_contour_i.geometry.apply(mapping), watershed_contour_i.crs)
    ZHp_ratio = compute_ZHp_ratio(ZHp_within_bv)
    intermittent_ratio = compute_intermittent_ratio(troncon_hydro_test)
    drainage_density_i = compute_drainage_density(watershed_contour_i, troncon_hydro_test)
    geomorpho_indicators = geomorpho_indicators.append({'Code':i, 'ZHp_ratio': ZHp_ratio, 'intermittent_ratio': intermittent_ratio, 'drainage_density': drainage_density_i}, ignore_index=True)



