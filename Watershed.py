import geopandas as gpd


class Watershed(object):
    """ This Watershed class enables to create a watershed object with all these properties (intrinsic et extrinsic)"""

    def __init__(self, code, name=-1, contour=-1, hydrological_signatures=-1, geologic_properties=-1, geomorphologic_properties=-1,
                 topographic_properties=-1, hydro_climatic_fluxes=-1):
        self.code = code
        self.name = name
        self.contour = contour
        self.hydrological_signatures = hydrological_signatures
        self.geologic_properties = geologic_properties
        self.geomorphologic_properties = geomorphologic_properties
        self.topographic_properties = topographic_properties
        self.hydro_climatic_fluxes = hydro_climatic_fluxes

    def to_dict(self):
        watershed_dict = {'name': self.name, 'code': self.code}
        watershed_dict.update(self.hydrological_signatures.__dict__)
        # watershed_dict.update(self.geologic_properties.__dict__)
        # watershed_dict.update(self.geomorphologic_properties.__dict__)
        # watershed_dict.update(self.topographic_properties.__dict__)
        return watershed_dict

    def extract_watershed_contour_from_filename(self, filepath, code_column_name='Code'):
        shpfile_contour = gpd.read_file(filepath)
        self.extract_watershed_contour(shpfile_contour, code_column_name)

    def extract_watershed_contour(self, shpfile_contour, code_column_name='Code'):
        shpfile_contour = shpfile_contour[shpfile_contour.loc[:, code_column_name] == self.code]
        self.contour = shpfile_contour

    def extract_banquehydro_discharge_timeseries(self, foldername):
        from HydroClimaticFluxes import HydroClimaticFluxes
        self.hydro_climatic_fluxes = HydroClimaticFluxes(self.code)
        self.hydro_climatic_fluxes.extract_discharge_timeseries(foldername, type_data='BanqueHydro')
        self.hydro_climatic_fluxes.keep_clean_HydroYear()
        # self.hydro_climatic_fluxes.extract_discharge_timeseries(foldername, type_data='csv_file')

    def extract_SAFRAN_forcings(self, foldername):
        from HydroClimaticFluxes import HydroClimaticFluxes
        if(self.hydro_climatic_fluxes==-1):
            self.hydro_climatic_fluxes = HydroClimaticFluxes(self.code)
        self.hydro_climatic_fluxes.extract_safran_timeseries_from_contour(foldername, self.contour)

    def extract_hydrological_signatures(self, option='all', adimensioned='no'):
        from HydrologicalSignatures import HydrologicalSignatures
        self.hydrological_signatures = HydrologicalSignatures()
        df_discharge = self.hydro_climatic_fluxes.discharge_timeseries
        if(adimensioned=='yes'):
            from numpy import nanmedian
            df_discharge.Q = df_discharge.Q/nanmedian(df_discharge.Q)
        if (option == 'all'):
            self.hydrological_signatures.from_discharge_time_series(df_discharge,'Q')
            df_obs_mod = self.hydro_climatic_fluxes.merge_time_series(merging_option='inner')
            self.hydrological_signatures.from_discharge_and_climate_time_series(df_obs_mod, 'Q', 'Ptot')
            self.hydrological_signatures.from_climate_time_series(self.hydro_climatic_fluxes.safran_timeseries,
                                                                  'Ptot', 'ET0')
        elif (option=='discharge_based'):
            self.hydrological_signatures.from_discharge_time_series(df_discharge,'Q')

    def extract_geologic_properties_from_filename(self, geol_foldername):
        geol_shp = gpd.read_file(geol_foldername)
        self.extract_geologic_properties(geol_shp)

    def extract_geologic_properties(self, geol_shp):
        from GeologicProperties import GeologicProperties
        self.geologic_properties = GeologicProperties()
        self.geologic_properties.extract_main_geology(geol_shp)
        # self.geologic_properties.extract_main_geology(geol_shp, self.contour)

    @staticmethod
    def test():
        watershed_name = 'Le Meaudret à Méaudre'
        code = 'W3315010'
        # shp_contour_filepath = '/home/jean.marcais/Donnees/DonneesLaura/BanqueHydro/Shapes/BassinsVersantsMetropole' \
        #                        '/BV_4207_stations.shp'
        shp_contour_filepath = './TestData/BH/W3315010_contour.shp'
        watershed_test = Watershed(code, watershed_name)
        watershed_test.extract_watershed_contour_from_filename(shp_contour_filepath, 'Code')
        # banquehydro_foldername = '/home/jean.marcais/Donnees/BanqueHydro/'
        banquehydro_foldername = './TestData/BH/'
        watershed_test.extract_banquehydro_discharge_timeseries(banquehydro_foldername)
        # safran_foldername = '/home/jean.marcais/Donnees/SAFRAN/SAFRAN_Vidal/'
        safran_foldername = 'C:/Users/laura.lindeperg/Documents/DonneesLaura/SAFRAN/daily/'
        watershed_test.extract_SAFRAN_forcings(safran_foldername)
        watershed_test.extract_hydrological_signatures()

        # BDLisa_shp = '/home/jean.marcais/Donnees/DonneesLaura/BD_Lisa/BD_Lisa_regionalhydrogeology.shp'
        BDLisa_shp = './TestData/GEOL/BDLisa_regional_hydrology_sample.shp'
        watershed_test.extract_geologic_properties_from_filename(BDLisa_shp)

        # BRGM_geol_map = '/home/jean.marcais/Donnees/BRGM/1M/GEO001M_CART_FR_S_FGEOL_2154.shp'
        BRGM_geol_map = './TestData/GEOL/GEO001M_CART_FR_S_FGEOL_2154_sample.shp'
        watershed_test.geologic_properties.extract_average_age_geology(BRGM_geol_map, watershed_test.contour)
        watershed_dict = watershed_test.to_dict()
        return watershed_test

    @staticmethod
    def test_hydrosignatures_Jean():
        watershed_name = 'Blabla'
        code_list = ['A1072010', 'A1080330', 'A3472010', 'I0102010', 'J3413030']
        shp_contour_filepath = '/home/jean.marcais/Donnees/DonneesLaura/BanqueHydro/Shapes/BassinsVersantsMetropole' \
                                '/BV_4207_stations.shp'
        banquehydro_foldername = '/home/jean.marcais/Donnees/BanqueHydro/'
        safran_foldername = '/home/jean.marcais/Donnees/SAFRAN/SAFRAN_Vidal/'
        watersheds=[]
        for code in code_list:
            print(code)
            watershed_test = Watershed(code, watershed_name)
            watershed_test.extract_watershed_contour_from_filename(shp_contour_filepath, 'Code')
            watershed_test.extract_banquehydro_discharge_timeseries(banquehydro_foldername)
            watershed_test.extract_SAFRAN_forcings(safran_foldername)
            watershed_test.extract_hydrological_signatures()
            watersheds.append(watershed_test)

        return watersheds

